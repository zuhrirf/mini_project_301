package com.miniproject.mp301.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonFormat;

@MappedSuperclass
public class BasePropertiesModel {
	@OneToOne
	@JoinColumn(name= "created_by", insertable = false, updatable = false)
	private MUserModel createByUser;
	
	@Column(nullable = false)
	private Long created_by;
	
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
	@Column(nullable = false)
	private Date created_on;
	
	@OneToOne
	@JoinColumn(name= "modified_by", insertable = false, updatable = false)
	private MUserModel modifiedByUser;
	
	private Long modified_by;
	
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
	private Date modified_on;
	
	@OneToOne
	@JoinColumn(name= "deleted_by", insertable = false, updatable = false)
	private MUserModel deletedByUser;
	
	private Long deleted_by;
	
	@JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
	private Date deleted_on;
	
	@Column(nullable = false)
	private Boolean is_deleted = false;

	public MUserModel getCreateByUser() {
		return createByUser;
	}

	public void setCreateByUser(MUserModel createByUser) {
		this.createByUser = createByUser;
	}

	public Long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Long created_by) {
		this.created_by = created_by;
	}

	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public MUserModel getModifiedByUser() {
		return modifiedByUser;
	}

	public void setModifiedByUser(MUserModel modifiedByUser) {
		this.modifiedByUser = modifiedByUser;
	}

	public Long getModified_by() {
		return modified_by;
	}

	public void setModified_by(Long modified_by) {
		this.modified_by = modified_by;
	}

	public Date getModified_on() {
		return modified_on;
	}

	public void setModified_on(Date modified_on) {
		this.modified_on = modified_on;
	}

	public MUserModel getDeletedByUser() {
		return deletedByUser;
	}

	public void setDeletedByUser(MUserModel deletedByUser) {
		this.deletedByUser = deletedByUser;
	}

	public Long getDeleted_by() {
		return deleted_by;
	}

	public void setDeleted_by(Long deleted_by) {
		this.deleted_by = deleted_by;
	}

	public Date getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(Date deleted_on) {
		this.deleted_on = deleted_on;
	}

	public Boolean getIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(Boolean is_deleted) {
		this.is_deleted = is_deleted;
	}
	

	
}
