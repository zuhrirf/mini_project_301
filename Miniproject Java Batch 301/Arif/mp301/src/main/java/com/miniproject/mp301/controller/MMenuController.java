package com.miniproject.mp301.controller;

import javax.servlet.http.HttpSession;

import com.miniproject.mp301.model.MUserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.miniproject.mp301.service.MUserService;

@Controller
public class MMenuController {
	@Autowired
	private MUserService mUserService;

	@RequestMapping("")
	public String viewLandingPage(HttpSession session) {
		return "landingPage.html";
	}

	@RequestMapping("/register")
	public String signup(HttpSession session) {
		return "landingPage.html";
	}

	@RequestMapping("/login")
	public String login(String email, String password, HttpSession session) {
		MUserModel user = mUserService.login(email, password);
		if (user != null) {
			// variabel yang diisi user
			session.setAttribute("id", user.getId());
			session.setAttribute("email", email);
			session.setAttribute("role", user.getRole().getName());
			session.setAttribute("name", user.getBiodata().getFullname());
			session.setAttribute("biodataId", user.getBiodata().getId());
			session.setAttribute("roleId", user.getRole().getId());
			return "menu.html";
		} else {
			return "landingPage.html";
		}
	}

	@RequestMapping("treatment")
	public String viewtreatmentPage(Model model, HttpSession session) {
		String name = (String) session.getAttribute("name");
		String role = (String) session.getAttribute("role");
		Long id = (Long) session.getAttribute("id");
		Long biodataId = (Long) session.getAttribute("biodataId");

		if (id != null) {
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			return "tindakan/tindakan.html";
		} else {
			return "landingPage.html";
		}

//		return "tindakan/tindakan.html";
	}

	@RequestMapping("profile")
	public String viewProfilePage(Model model, HttpSession session) {
		String name = (String) session.getAttribute("name");
		String role = (String) session.getAttribute("role");
		Long id = (Long) session.getAttribute("id");
		Long biodataId = (Long) session.getAttribute("biodataId");

		if (id != null) {
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			return "profile/profile.html";
		} else {
			return "landingPage.html";
		}

//		return "tindakan/tindakan.html";
	}

	@RequestMapping("menu")
	public String viewDashboard(Model model, HttpSession session) {
		String email = (String) session.getAttribute("email");
		String name = (String) session.getAttribute("name");
		String role = (String) session.getAttribute("role");
		Long id = (Long) session.getAttribute("id");
		Long biodataId = (Long) session.getAttribute("biodataId");
		Long roleId = (Long) session.getAttribute("roleId");

		if (email != null) {
			model.addAttribute("email", email);
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			model.addAttribute("roleId", roleId);
			return "menu.html";
		} else {
			return "landingPage.html";
		}
	}

	@RequestMapping("/logout")
	public String logOut(HttpSession session) {
		// hancurkan session
		session.invalidate();
		return "landingPage.html";
	}
}
