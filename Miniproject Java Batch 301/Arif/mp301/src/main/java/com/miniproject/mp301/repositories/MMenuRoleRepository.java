package com.miniproject.mp301.repositories;

import java.util.List;

import com.miniproject.mp301.model.MMenuRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MMenuRoleRepository extends JpaRepository<MMenuRole, Long> {
	@Query(value = "SELECT * FROM m_menu_role WHERE role_id = :role_id", nativeQuery = true)
	List<MMenuRole> getMenuByRoleId(Long role_id);

	@Query(value = "SELECT * FROM m_menu_role mr join m_menu m on mr.menu_id = m.id where role_id = :role_id AND parent_id ISNULL", nativeQuery = true)
	List<MMenuRole> getMainMenuByRoleId(Long role_id);
	
	@Query(value = "SELECT * FROM m_menu_role mr join m_menu m on mr.menu_id = m.id where role_id = :role_id AND parent_id = :parent_id", nativeQuery = true)
	List<MMenuRole> getSubMenuByRoleId(Long role_id, Long parent_id);

}
