package com.miniproject.mp301.repositories;

import com.miniproject.mp301.model.MRoleModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MRoleRepository extends JpaRepository<MRoleModel, Long> {

}
