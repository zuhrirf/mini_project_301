package com.miniproject.mp301.repositories;

import com.miniproject.mp301.model.MBiodataModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MBiodataRepository extends JpaRepository<MBiodataModel, Long> {

}
