package com.miniproject.mp301.controller;

import java.util.List;

import com.miniproject.mp301.model.MMenuRole;
import com.miniproject.mp301.service.MMenuRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/menu")
public class MMenuRoleRestController {
	@Autowired
	private MMenuRoleService mMenuRoleService;
	
	@GetMapping("getmenubyroleid/{roleId}")
	public List<MMenuRole> getMenuByRoleId(@PathVariable("roleId") Long role_id){
		return mMenuRoleService.getMenuByRoleId(role_id);
	}
	
	@GetMapping("getmainmenubyroleid/{roleId}")
	public List<MMenuRole> getMainMenuByRoleId(@PathVariable("roleId") Long role_id){
		return mMenuRoleService.getMainMenuByRoleId(role_id);
	}
	
	@GetMapping("getsubmenubyroleid")
	public List<MMenuRole> getSubMenuByRoleId(@RequestParam("roleId") Long role_id, @RequestParam("parentId") Long parent_id){
		return mMenuRoleService.getSubMenuByRoleId(role_id, parent_id);
	}
}
