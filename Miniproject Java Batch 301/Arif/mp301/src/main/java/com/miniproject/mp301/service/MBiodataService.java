package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MBiodataModel;
import com.miniproject.mp301.repositories.MBiodataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MBiodataService {
	@Autowired
	private MBiodataRepository mBiodataRepository;
	
	public MBiodataModel addBiodata(MBiodataModel biodata) {
		return mBiodataRepository.save(biodata);
	}
}
