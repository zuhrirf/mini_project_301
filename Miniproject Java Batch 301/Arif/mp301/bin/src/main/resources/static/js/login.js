$("#btnLogin").click(function() {
	var email = $("#typeEmail").val();
	var passwd = $("#typePassword").val();
	console.log(email, passwd);
	if (email == "") {
		$("#errEmail").text("Email Must be fill!");
		return;
	} else {
		$("#errEmail").text("");
	}

	if (passwd == "") {
		$("#errPasswd").text("Password Must be fill!");
		return;
	} else {
		$("#errPasswd").text("");
	}

	// hit API ajax
	$.ajax({
		url: "api/login/getAllUser",
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			console.log(data);
			for (i = 0; i < data.length; i++) {
				/*
				if (data[i].email != email) {
					$("#errEmail").text("Email salah");
				} else {
					$("#errEmail").text("");
				}
				*/

				if (data[i].email != email || data[i].password != passwd) {
					$("#errPasswd").text("Invaild password or email!");
				} else {
					$("#errPasswd").text("");
				}

				if (data[i].email == email && data[i].password == passwd) {
					$.ajax({
						url: "/login",
						type: "GET",
						contentType: "html",
						success: function() {
							window.location.href = "/dashboard";
						}
					});
				}
			}
		},
		error: function() {
			alert("Terjadi Kesalahan");
		}
	});
});



$("#forgetPasswd").click(function() {
	$.ajax({
		url: "/forget",
		type: "GET",
		contentType: "html",
		success: function(data) {
			/*
			$(".modal-title").text("Forget Password");
			$(".modal-body").html(data);
			*/
			$("#forgetModal").modal("show");
		}
	});
});

$("#btnSendOTP").click(function() {
	$("#forgetModal").modal("hide");
	$("#verifyModal").modal("show");
});

$("#btnConfirmOTP").click(function() {
	$("#verifyModal").modal("hide");
	$("#resetModal").modal("show");

});

$("#btnSetPassword").click(function() {
	$("#resetModal").modal("hide");
});
