package com.miniproject.mp301.model;


import javax.persistence.*;

@Entity
@Table(name="m_location")
public class MLocationModel extends BasePropertiesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column()
    private String name;

    @Column()
    private Long location_level_id;

    @ManyToOne
    @JoinColumn(name = "locatio_level_id", insertable = false, updatable = false)
    private MLocationLevelModel mLocationLevelModel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLocation_level_id() {
        return location_level_id;
    }

    public void setLocation_level_id(Long location_level_id) {
        this.location_level_id = location_level_id;
    }

    public MLocationLevelModel getmLocationLevelModel() {
        return mLocationLevelModel;
    }

    public void setmLocationLevelModel(MLocationLevelModel mLocationLevelModel) {
        this.mLocationLevelModel = mLocationLevelModel;
    }
}
