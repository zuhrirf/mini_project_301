package com.miniproject.mp301.controller;

import com.miniproject.mp301.model.MUserModel;
import com.miniproject.mp301.service.MBiodataService;
import com.miniproject.mp301.service.MUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class DefaultController {

    @Autowired
    private MUserService mUserService;

    @Autowired
    private MBiodataService mBiodataService;

    @RequestMapping("mainpage")
    public String viewLandingPage(HttpSession httpSession) {
        return "mainpage.html";
    }


    @RequestMapping("profile")
    public String viewProfilePage(Model model, HttpSession httpSession) {
        String name = (String) httpSession.getAttribute("name");
        String role = (String) httpSession.getAttribute("role");
        Long id = (Long) httpSession.getAttribute("id");
        Long biodata_id = (Long) httpSession.getAttribute("biodata_id");

        if (id != null) {
            model.addAttribute("name", name);
            model.addAttribute("role", role);
            model.addAttribute("id", id);
            model.addAttribute("biodata_id", biodata_id);
            return "profile/profile.html";
        } else {
            return "profile/profile.html";
        }
    }


    @RequestMapping("alamat")
    public String viewDashboard(Model model, HttpSession httpSession) {
        String name = (String) httpSession.getAttribute("name");
        String role = (String) httpSession.getAttribute("role");
        Long id = (Long) httpSession.getAttribute("id");
        Long biodata_id = (Long) httpSession.getAttribute("biodata_id");

        if (id != null) {
            model.addAttribute("name", name);
            model.addAttribute("role", role);
            model.addAttribute("id", id);
            model.addAttribute("biodata_id", biodata_id);
            return "alamat/alamat.html";
        } else {
            return "alamat/alamat.html";
        }
    }
}
