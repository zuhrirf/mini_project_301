package com.miniproject.mp301.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="m_role")
public class MRoleModel extends BasePropertiesModel {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 20)
    private String name;

    @Column(length = 20)
    private String code;

    @Column(nullable = false)
    private Long created_by;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date create_on;

    @Column(nullable = false)
    private Long modified_by;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date modified_on;

    @Column(nullable = false)
    private Long delete_by;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date delete_on;

    @Column
    private Boolean is_delete;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }
}