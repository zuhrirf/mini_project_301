package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MRoleModel;
import com.miniproject.mp301.repository.MRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MRoleService {
    @Autowired
    private MRoleRepository mRoleRepository;

    public List<MRoleModel> getAllRole(){
        return mRoleRepository.findAll();
    }
}
