package com.miniproject.mp301.controller;

import com.miniproject.mp301.model.MBiodataModel;
import com.miniproject.mp301.service.MBiodataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class MBiodataRestController {

    @Autowired
    private MBiodataService mBiodataService;

    @GetMapping("getallbiodata")
    public List<MBiodataModel> getAllBiodata() {
        return mBiodataService.getAllBiodata();
    }

    @GetMapping("getbiodata/{id}")
    public MBiodataModel getBiodataById(@PathVariable("id") Long id) {
        return mBiodataService.getBiodataById(id);
    }

}
