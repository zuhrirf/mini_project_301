package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MBiodataModel;
import com.miniproject.mp301.repository.MBiodataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MBiodataService {
    @Autowired
    private MBiodataRepository mBiodataRepository;

    public List<MBiodataModel> getAllBiodata(){
        return mBiodataRepository.findAll();
    }

    public MBiodataModel getBiodataById(Long id) {
        return mBiodataRepository.findById(id).get();
    }
}
