package com.miniproject.mp301.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import java.util.Date;

public class BasePropertiesModel {
    @Column(nullable = false)
    private Long created_by;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date created_on;

    @Column(nullable = false)
    private Long modified_by;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date modified_on;

    @Column(nullable = false)
    private Long delete_by;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date delete_on;

    @Column(nullable = false)
    private Boolean is_delete;

    public Long getCreated_by() {
        return created_by;
    }

    public void setCreated_by(Long created_by) {
        this.created_by = created_by;
    }

    public Date getCreate_on() {
        return created_on;
    }

    public void setCreate_on(Date create_on) {
        this.created_on = create_on;
    }

    public Long getModified_by() {
        return modified_by;
    }

    public void setModified_by(Long modified_by) {
        this.modified_by = modified_by;
    }

    public Date getModified_on() {
        return modified_on;
    }

    public void setModified_on(Date modified_on) {
        this.modified_on = modified_on;
    }

    public Long getDelete_by() {
        return delete_by;
    }

    public void setDelete_by(Long delete_by) {
        this.delete_by = delete_by;
    }

    public Date getDelete_on() {
        return delete_on;
    }

    public void setDelete_on(Date delete_on) {
        this.delete_on = delete_on;
    }

    public Boolean getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(Boolean is_delete) {
        this.is_delete = is_delete;
    }
}
