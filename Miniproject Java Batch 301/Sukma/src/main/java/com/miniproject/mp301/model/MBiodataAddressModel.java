package com.miniproject.mp301.model;

import javax.persistence.*;

@Entity
@Table(name = "m_biodata_address")
public class MBiodataAddressModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column()
    private Long biodata_id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private MBiodataModel mBiodataModel;

    @Column(length = 100)
    private String label;

    @Column(length = 100)
    private String recipient;

    @Column(length = 15)
    private String recipient_phone_number;

    @Column()
    private Long location_id;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    private MLocationModel mLocationModel;

    @Column(length = 10)
    private String postal_code;

    @Column()
    private String address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Long biodata_id) {
        this.biodata_id = biodata_id;
    }

    public MBiodataModel getmBiodataModel() {
        return mBiodataModel;
    }

    public void setmBiodataModel(MBiodataModel mBiodataModel) {
        this.mBiodataModel = mBiodataModel;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipient_phone_number() {
        return recipient_phone_number;
    }

    public void setRecipient_phone_number(String recipient_phone_number) {
        this.recipient_phone_number = recipient_phone_number;
    }

    public Long getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Long location_id) {
        this.location_id = location_id;
    }

    public MLocationModel getmLocationModel() {
        return mLocationModel;
    }

    public void setmLocationModel(MLocationModel mLocationModel) {
        this.mLocationModel = mLocationModel;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
