package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MUserModel;
import com.miniproject.mp301.repository.MUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MUserService {
    @Autowired
    private MUserRepository mUserRepository;

    public MUserModel login(String email, String password) {
        return mUserRepository.login(email, password);
    }

    public List<MUserModel> getAllUser() {
        return mUserRepository.findAll();
    }

    public MUserModel getUserById(Long id) {
        return mUserRepository.findById(id).get();
    }

    public MUserModel addUser(MUserModel user) {
        return mUserRepository.save(user);
    }

    public MUserModel editUser(MUserModel user) {
        return mUserRepository.save(user);
    }

    // Search By Email
    public MUserModel searchEmail(String email) {
        return mUserRepository.searchEmail(email);
    }

    // Search By Password
    public MUserModel searchPassword(String password) {
        return mUserRepository.searchPassword(password);
    }

    public void updateLastLogin(Date lastLogin, String email) {
        mUserRepository.updatelastLogin(lastLogin, email);
    }

    public void updateLoginAttempt(Integer loginAttempt, String email) {
        mUserRepository.updateLoginAttempts(loginAttempt, email);
    }

    public void updateIsLock(Boolean lock, String email) {
        mUserRepository.updateIsLock(lock, email);
    }
}
