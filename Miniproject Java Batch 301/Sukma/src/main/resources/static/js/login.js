$(document).ready(function() {
	// Eye Icon Lihat Password
	$("#show_hide_password a").on('click', function(event) {
		event.preventDefault();
		if ($('#show_hide_password input').attr("type") == "text") {
			$('#show_hide_password input').attr('type', 'password');
			$('#show_hide_password i').addClass("fa-eye-slash");
			$('#show_hide_password i').removeClass("fa-eye");
		} else if ($('#show_hide_password input').attr("type") == "password") {
			$('#show_hide_password input').attr('type', 'text');
			$('#show_hide_password i').removeClass("fa-eye-slash");
			$('#show_hide_password i').addClass("fa-eye");
		}
	});

	$("#show_hide_new_password a").on('click', function(event) {
		event.preventDefault();
		if ($('#show_hide_new_password input').attr("type") == "text") {
			$('#show_hide_new_password input').attr('type', 'password');
			$('#show_hide_new_password i').addClass("fa-eye-slash");
			$('#show_hide_new_password i').removeClass("fa-eye");
		} else if ($('#show_hide_new_password input').attr("type") == "password") {
			$('#show_hide_new_password input').attr('type', 'text');
			$('#show_hide_new_password i').removeClass("fa-eye-slash");
			$('#show_hide_new_password i').addClass("fa-eye");
		}
	});

	$("#show_hide_new_reenter_password a").on('click', function(event) {
		event.preventDefault();
		if ($('#show_hide_new_reenter_password input').attr("type") == "text") {
			$('#show_hide_new_reenter_password input').attr('type', 'password');
			$('#show_hide_new_reenter_password i').addClass("fa-eye-slash");
			$('#show_hide_new_reenter_password i').removeClass("fa-eye");
		} else if ($('#show_hide_new_reenter_password input').attr("type") == "password") {
			$('#show_hide_new_reenter_password input').attr('type', 'text');
			$('#show_hide_new_reenter_password i').removeClass("fa-eye-slash");
			$('#show_hide_new_reenter_password i').addClass("fa-eye");
		}
	});
});

$("#btnLoginFirst").click(function() {
	$("#typeEmail").val("");
	$("#typePassword").val("");
	$("#errEmail").text("");
	$("#errPasswd").text("");
	$("#loginModal").modal("show");
});


// Click Buttton Login Halaman Landing Page
$("#btnLogin").click(function() {
	var email = $("#typeEmail").val();
	var passwd = $("#typePassword").val();
	console.log(email, passwd);

	// Validasi login berdasarkan email dan password
	if (email == "") {
		$("#errEmail").text("Email Must be fill!");
		return;
	} else {
		$("#errEmail").text("");
	}

	if (passwd == "") {
		$("#errPasswd").text("Password Must be fill!");
		return;
	} else {
		$("#errPasswd").text("");
	}


	// Hit Api login berdasarkan email dan password
	$.ajax({
		url: "/api/login/emailAndPassword?email=" + email + "&" + "password=" + passwd,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			//console.log(data);
			if (data.email != email && data.password != passwd) {
				alert("Invalid password or email!");
				$("#typeEmail").val("");
				$("#typePassword").val("");
			}

			if (data.is_locked == true) {
				alert("Account is locked, because you already stock out login attempt.")
				$("#typeEmail").val("");
				$("#typePassword").val("");
			}

			else if (data.is_locked == false) {
				if (data.email == email && data.password == passwd) {
					$.ajax({
						url: "/login",
						type: "GET",
						data: {
							id: data.id,
							email: data.email,
							password: data.password,
							name: data.biodata.fullname,
							role: data.role.name
						},
						contentType: "html",
						success: function() {
							window.location.href = "/menu";
						}
					});
				}
			}
		},
		error: function() {
			alert("Error");
		}
	});

});



// Click button create account modal login
$("#createAccount").click(function() {
	$("#typeEmailRegister").val("");
	$("#errEmailRegister").text("");
	$("#registerModal").modal("show");
	$("#loginModal").modal("hide");
});

// Click button forgot password modal login
$("#forgetPasswd").click(function() {
	$("#typeEmailForget").val("");
	$("#errEmailForget").text("");
	$("#forgetModal").modal("show");
	$("#loginModal").modal("hide");
});

// Generated OTP 6 digit
function generateOTP() {
	var digits = '0123456789';
	let OTP = '';
	for (let i = 0; i < 6; i++) {
		OTP += digits[Math.floor(Math.random() * 10)];
	}
	return OTP;
}

// Click button send OTP di veriv email
var emailForget;
var spn = document.getElementById("count");
//console.log(spn);
var count = 10;     // Set count
var timer = null;  // For referencing the timer

$("#btnSendOTP").click(function() {
	emailForget = $("#typeEmailForget").val();
	if (emailForget == "") {
		$("#errEmailForget").text("Email Must be fill!");
		return;
	} else {
		$("#errEmailForget").text("");
	}

	// Validasi Email sudah ada apa belum
	$.ajax({
		url: "/api/login/email?email=" + emailForget,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			//console.log(data);
			if (data.email != emailForget) {
				$("#errEmailForget").text("Wrong Email");
			} else {
				$("#errEmailForget").text("");
			}
			if (data.email == emailForget) {

				// Send kode otp ke email
				var recipient = $("#typeEmailForget").val();

				var obj = {};
				obj.email = recipient;
				obj.token = generateOTP();

				var myJson = JSON.stringify(obj);
				console.log(myJson);

				$.ajax({
					url: "/api/token/sendMail",
					type: "POST",
					contentType: "application/json",
					data: myJson,
					success: function(data) {
						//console.log(data);
						if (data == "Successful") {


							alert("Success Send Email");

							// Countdown Timer
							// Get refreence to span and button
							(function countDown() {
								// Display counter and start counting down
								spn.textContent = count;

								var minutes = parseInt(count / 60, 10);
								var seconds = parseInt(count % 60, 10);

								minutes = minutes < 10 ? "0" + minutes : minutes;
								seconds = seconds < 10 ? "0" + seconds : seconds;

								spn.textContent = minutes + ":" + seconds;
								// Run the function again every second if the count is not zero
								if (count !== 0) {
									timer = setTimeout(countDown, 1000);
									count--; // decrease the timer
								} else {
									$("#btnCounter").css("color", "DodgerBlue");
									$("#btnCounter").css("cursor", "pointer");

									// Enable the button, Button kirim ulang
									$("#btnCounter").click(function() {
										$.ajax({
											url: "/api/token/setExpiredTokenAllEmail?email=" + emailForget,
											type: "PUT",
											contentType: "application/json"
										});
										var obj = {};
										obj.email = emailForget;
										obj.token = generateOTP();

										var myJson = JSON.stringify(obj);
										console.log(myJson);

										$.ajax({
											url: "/api/token/sendMail",
											type: "POST",
											contentType: "application/json",
											data: myJson,
											success: function(data) {
												//console.log(data);
												if (data == "Successful") {
													alert("Success Send Email");
													// Hit Api InsertToken
													$.ajax({
														url: "/api/token/insertToken",
														type: "POST",
														contentType: "application/json",
														data: myJson,
														success: function() { },
														error: function() {
															alert("Terjadi Kesalahan");
														}
													});
												} else {
													alert(data);
												}
											},
											error: function() {
												alert("Terjadi Kesalahan");
											}
										});
									});
								}
							}());


							// Hit Api InsertToken
							$.ajax({
								url: "/api/token/insertToken",
								type: "POST",
								contentType: "application/json",
								data: myJson,
								success: function(data) {
									//console.log(data);
									if (data == "ok") {
										$("#typeEmailForget").val("");
										$("#forgetModal").modal("hide");
										$("#verifyModal").modal("show");
									}
								},
								error: function() {
									alert("Terjadi Kesalahan");
								}
							});
						} else {
							alert(data);
						}

					},

					error: function() {
						alert("Terjadi Kesalahan");
					}
				});
			}

		},
		error: function() {
			alert("Terjadi Kesalahan");
		}
	});
});


// Click Button Confirm OTP
$("#btnConfirmOTP").click(function() {
	var otp = $("#typeOTP").val();
	//console.log(otp);
	if (otp == "") {
		$("#errOTP").text("OTP Must be fill!");
		return;
	} else {
		$("#errOTP").text("");
	}

	// hit API ajax
	$.ajax({
		url: "api/token/getOtpByTokenAndEmail?token=" + otp + "&email=" + emailForget,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			//console.log(data);
			if (data.token != otp) {
				$("#errOTP").text("Invalid OTP!");
			}

			if (data.is_expired == true) {
				alert("Please resend OTP, because your time was out")
				$("#typeEmail").val("");
				$("#typePassword").val("");

			} else if (data.is_expired == false && data.email == emailForget) {
				if (data.token == otp) {
					$("#errOTP").text("");
					$("#typeOTP").val("");
					$("#verifyModal").modal("hide");
					$("#resetModal").modal("show");
				}
			}
		},
		error: function() {
			alert("Terjadi Kesalahan");
		}
	});
});

// Click Button Set Password
$("#btnSetPassword").click(function() {
	var newPasswd = $("#typePasswordReset").val().trim();
	var newRePasswd = $("#typeRePasswordReset").val().trim();
	const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/;
	if (newRePasswd == "" || newPasswd == "") {
		$("#errRePasswdReset").text("Paswword must be fill!");
		//console.log("kosong", newPasswd, newRePasswd);
	} else if (newPasswd != newRePasswd) {
		$("#errRePasswdReset").text("Passwword don't match!");
		//console.log(newPasswd, newRePasswd);
	} else if (!newPasswd.match(pattern) && !newRePasswd.match(pattern)) {
		//alert("salah");
		$("#errRePasswdReset").text("Please fill with request format");
	} else {
		$("#errRePasswdReset").text("");
		var resetPassword = {};
		resetPassword.new_password = newPasswd;

		var user = {};
		user.email = emailForget

		var obj = { resetPassword, user };

		var myJson = JSON.stringify(obj);
		//console.log(myJson);

		$.ajax({
			url: "/api/reset/resetPassword",
			type: "POST",
			contentType: "application/json",
			data: myJson,
			success: function(data) {
				if (data == "ok") {
					$("#typePasswordReset").val("");
					$("#typeRePasswordReset").val("");
					$("#errRePasswdReset").text("");
					$("#resetModal").modal("hide");
					$("#successModal").modal("show");
				}
			},
			error: function() {
				alert("Terjadi Kesalahan");
			}
		});
	}
});