package com.miniproject.mp301.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.mp301.models.TDoctorTreatmentModel;

public interface DoctorTreatmentRepository extends JpaRepository<TDoctorTreatmentModel, Long>{
	
	@Query(value = "UPDATE t_doctor_treatment SET is_deleted = :isDeleted, deleted_on= :deletedOn WHERE id = :id", nativeQuery = true)
	@Modifying
	void updateIsDeleted(Date deletedOn, Boolean isDeleted, Long id);
	
	@Query(value = "SELECT * FROM t_doctor_treatment WHERE doctor_id = :doctor_id", nativeQuery = true)
	List<TDoctorTreatmentModel> getTreatmentsByDoctorId(Long doctor_id);
}