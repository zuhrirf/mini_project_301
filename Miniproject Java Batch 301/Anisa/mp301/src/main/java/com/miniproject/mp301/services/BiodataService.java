package com.miniproject.mp301.services;

import com.miniproject.mp301.repositories.BiodataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.miniproject.mp301.models.M_Biodata;

@Service
@Transactional
public class BiodataService {
	@Autowired
	private BiodataRepository biodataRepo;
	
	public M_Biodata addBiodata(M_Biodata biodata) {
		return biodataRepo.save(biodata);
	}
}
