package com.miniproject.mp301.services;

import java.util.Date;
import java.util.List;

import com.miniproject.mp301.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.miniproject.mp301.models.M_User;

@Service
@Transactional
public class UserService {
	@Autowired
	private UserRepository userRepo;

	public M_User login(String email, String password) {
		return userRepo.login(email, password);
	}

	public List<M_User> getAllUser() {
		return userRepo.findAll();
	}

	public M_User getUserById(Long id) {
		return userRepo.findById(id).get();
	}
	
	public M_User addUser(M_User user) {
		return userRepo.save(user);
	}
	
	public M_User editUser(M_User user) {
		return userRepo.save(user);
	}

	// Search By Email
	public M_User searchEmail(String email) {
		return userRepo.searchEmail(email);
	}

	// Search By Password
	public M_User searchPassword(String password) {
		return userRepo.searchPassword(password);
	}
	
	public void updateLastLogin(Date lastLogin, String email) {
		userRepo.updatelastLogin(lastLogin, email);
	}
	
	public void updateLoginAttempt(Integer loginAttempt, String email) {
		userRepo.updateLoginAttempts(loginAttempt, email);
	}
	
	public void updateIsLock(Boolean lock, String email) {
		userRepo.updateIsLock(lock, email);
	}
	
}
