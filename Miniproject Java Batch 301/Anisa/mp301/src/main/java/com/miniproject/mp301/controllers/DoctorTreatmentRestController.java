package com.miniproject.mp301.controllers;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.models.TDoctorTreatmentModel;
import com.miniproject.mp301.services.DoctorTreatmentService;

@RestController
@RequestMapping("api/doctorTreatment")
public class DoctorTreatmentRestController {
	@Autowired
	private DoctorTreatmentService doctorTreatmentServe;

	@GetMapping("getAllTreatment")
	public List<TDoctorTreatmentModel> getAllTreatment() {
		List<TDoctorTreatmentModel> treatments = doctorTreatmentServe.getAllTreatment();

		// Java stream sorting & Filter
		List<TDoctorTreatmentModel> filterTreatment = treatments.stream()
//				.sorted((id1, id2) -> (int) (id1.getId().compareTo(id2.getId())))
				.sorted(Comparator.comparingLong(TDoctorTreatmentModel::getId))
				.filter(treatment -> treatment.getIs_deleted() == false).collect(Collectors.toList());

		return filterTreatment;
//		return doctorTreatmentServe.getAllTreatment();
	}

	@GetMapping("getTreatmentById/{id}")
	public TDoctorTreatmentModel getTreatmentById(@PathVariable("id") Long id) {
		return doctorTreatmentServe.getTreatmentById(id);
	}
	
	@GetMapping("getTreatmentByDoctorId/{doctorId}")
	public List<TDoctorTreatmentModel> getTreatmentByDoctorId(@PathVariable("doctorId") Long doctor_id) {
		List<TDoctorTreatmentModel> treatments = doctorTreatmentServe.getTreatmentByDoctor_id(doctor_id);

		// Java stream sorting & Filter
		List<TDoctorTreatmentModel> filterTreatment = treatments.stream()
//				.sorted((id1, id2) -> (int) (id1.getId().compareTo(id2.getId())))
				.sorted(Comparator.comparingLong(TDoctorTreatmentModel::getId))
				.filter(treatment -> treatment.getIs_deleted() == false).collect(Collectors.toList());

		return filterTreatment;
	}

	@PostMapping("addTreatment")
	public String addTreatment(@RequestBody TDoctorTreatmentModel doctorTreatment) {
		List<TDoctorTreatmentModel> treatment = doctorTreatmentServe.getTreatmentByDoctor_id(doctorTreatment.getDoctor_id());
		boolean isAnyElementMatch = treatment.stream()
				.anyMatch(x -> x.getName().equalsIgnoreCase(doctorTreatment.getName()) && x.getIs_deleted()==false);

		if (isAnyElementMatch) {
			return "Treatment is exist";
		} else {
			doctorTreatment.setCreated_by(1L);
			doctorTreatment.setCreated_on(new Date());
			doctorTreatmentServe.addTreatment(doctorTreatment);
			return "ok";
		}
	}

	@DeleteMapping("deleteTreatment/{id}")
	public String deleteTreatment(@PathVariable("id") Long id) {

		doctorTreatmentServe.deleteTreatmentById(id);

		return "ok";
	}

	@PostMapping("softDeleteTreatment/{id}")
	public String softDeleteTreatmentById(@PathVariable("id") Long id) {
		TDoctorTreatmentModel treatmentById = doctorTreatmentServe.getTreatmentById(id);

		if (treatmentById != null) {
			treatmentById.setIs_deleted(true);
			treatmentById.setDeleted_on(new Date());
			doctorTreatmentServe.updateIsDeleted(treatmentById.getDeleted_on(), treatmentById.getIs_deleted(), id);
			return "ok";
		} else {
			return "error";
		}
	}
}