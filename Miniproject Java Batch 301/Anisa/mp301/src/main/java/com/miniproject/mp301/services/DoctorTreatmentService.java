package com.miniproject.mp301.services;

import java.util.Date;
import java.util.List;

import com.miniproject.mp301.repositories.DoctorTreatmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.miniproject.mp301.models.TDoctorTreatmentModel;

@Service
@Transactional
public class DoctorTreatmentService {
	@Autowired
	private DoctorTreatmentRepository docTreatRepo;
	
	public List<TDoctorTreatmentModel> getAllTreatment(){
		return docTreatRepo.findAll();
	}
	
	public TDoctorTreatmentModel getTreatmentById(Long id) {
		return docTreatRepo.findById(id).get();
	}
	
	public List<TDoctorTreatmentModel> getTreatmentByDoctor_id(Long doctor_id){
		return docTreatRepo.getTreatmentsByDoctorId(doctor_id);
	}
	
	public void addTreatment(TDoctorTreatmentModel doctorTreatment) {
		docTreatRepo.save(doctorTreatment);
	}
	
	public void deleteTreatmentById(Long id) {
		docTreatRepo.deleteById(id);
	}
	
	public void updateIsDeleted(Date deletedOn, Boolean isDeleted, Long id) {
		docTreatRepo.updateIsDeleted(deletedOn, isDeleted, id);
	}
	
}
