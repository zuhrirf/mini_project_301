package com.miniproject.mp301.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.miniproject.mp301.models.M_Menu_Role;

public interface MenuRoleRepository extends JpaRepository<M_Menu_Role, Long> {
	@Query(value = "SELECT * FROM m_menu_role WHERE role_id = :role_id", nativeQuery = true)
	List<M_Menu_Role> getMenusByRoleId(Long role_id);

	@Query(value = "SELECT * FROM m_menu_role mr join m_menu m on mr.menu_id = m.id where role_id = :role_id AND parent_id ISNULL", nativeQuery = true)
	List<M_Menu_Role> getMainMenusByRoleId(Long role_id);
	
	@Query(value = "SELECT * FROM m_menu_role mr join m_menu m on mr.menu_id = m.id where role_id = :role_id AND parent_id = :parent_id", nativeQuery = true)
	List<M_Menu_Role> getSubMenusByRoleId(Long role_id, Long parent_id);
	
}
