package com.miniproject.mp301.services;

import java.util.List;

import com.miniproject.mp301.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.miniproject.mp301.models.M_Role;

@Service
@Transactional
public class RoleService {
	@Autowired
	private RoleRepository roleRepo;
	
	public List<M_Role> getAllRoles(){
		return roleRepo.findAll();
	}
}
