package com.miniproject.mp301.services;

import java.util.List;

import com.miniproject.mp301.repositories.MenuRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.miniproject.mp301.models.M_Menu_Role;

@Service
@Transactional
public class MenuRoleService {
	@Autowired
	private MenuRoleRepository menuRoleRepo;
	
	public List<M_Menu_Role> getMenusByRoleId(Long role_id) {
		return menuRoleRepo.getMenusByRoleId(role_id);
	}
	
	public List<M_Menu_Role> getMainMenuByRoleId(Long role_id){
		return menuRoleRepo.getMainMenusByRoleId(role_id);
	}
	
	public List<M_Menu_Role> getSubMenuByRoleId(Long role_id, Long parent_id){
		return menuRoleRepo.getSubMenusByRoleId(role_id, parent_id);
	}
}
