package com.miniproject.mp301.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.miniproject.mp301.models.M_Role;

public interface RoleRepository extends JpaRepository<M_Role, Long> {

}
