package com.miniproject.mp301.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.models.M_Menu_Role;
import com.miniproject.mp301.services.MenuRoleService;

@RestController
@RequestMapping("api/menu")
public class MenuRoleRestController {
	@Autowired
	private MenuRoleService menuRoleServe;
	
	@GetMapping("getMenusByRoleId/{roleId}")
	public List<M_Menu_Role> getMenusByRoleId(@PathVariable("roleId") Long role_id){
		return menuRoleServe.getMenusByRoleId(role_id);
	}
	
	@GetMapping("getMainMenusByRoleId/{roleId}")
	public List<M_Menu_Role> getMainMenusByRoleId(@PathVariable("roleId") Long role_id){
		return menuRoleServe.getMainMenuByRoleId(role_id);
	}
	
	@GetMapping("getSubMenusByRoleId")
	public List<M_Menu_Role> getSubMenusByRoleId(@RequestParam("roleId") Long role_id, @RequestParam("parentId") Long parent_id){
		return menuRoleServe.getSubMenuByRoleId(role_id, parent_id);
	}
}
