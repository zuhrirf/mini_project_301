package com.miniproject.mp301.controllers;

//import com.miniproject.mp301.services.DoctorService;
import com.miniproject.mp301.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class DoctorController {

    //@Autowired
    //private DoctorService doctorServe;

    @Autowired
    private UserService userServe;

    @RequestMapping("profile")
    public String viewProfilePage(Model model, HttpSession s) {
        /*String name = (String) s.getAttribute("name");
        String role = (String) s.getAttribute("role");
        Long id = (Long) s.getAttribute("id");
        Long biodataId = (Long) s.getAttribute("biodataId");

        if (id != null) {
            model.addAttribute("name", name);
            model.addAttribute("role", role);
            model.addAttribute("id", id);
            model.addAttribute("biodataId", biodataId);
            return "profile/profile.html";
        } else {
            return "landingPage.html";
        }*/

        return "profile/profile.html";
    }
}
