package com.miniproject.mp301.services;

import com.miniproject.mp301.repositories.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.miniproject.mp301.models.MDoctorModel;

@Service
@Transactional
public class DoctorService {
	@Autowired
	private DoctorRepository doctorRepo;
	
	public MDoctorModel getDoctorByBiodata_id(Long biodataId) {
		return doctorRepo.getDoctorByBiodataId(biodataId);
	}
	
}
