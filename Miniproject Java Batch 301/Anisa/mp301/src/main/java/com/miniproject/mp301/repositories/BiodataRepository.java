package com.miniproject.mp301.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.miniproject.mp301.models.M_Biodata;

public interface BiodataRepository extends JpaRepository<M_Biodata, Long> {

}
