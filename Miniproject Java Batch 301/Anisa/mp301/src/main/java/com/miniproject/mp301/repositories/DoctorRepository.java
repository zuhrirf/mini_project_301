package com.miniproject.mp301.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.miniproject.mp301.models.MDoctorModel;

public interface DoctorRepository extends JpaRepository<MDoctorModel, Long> {
	@Query(value = "SELECT * FROM m_doctor WHERE biodata_id = :biodata_id", nativeQuery = true)
	MDoctorModel getDoctorByBiodataId(Long biodata_id);

}
