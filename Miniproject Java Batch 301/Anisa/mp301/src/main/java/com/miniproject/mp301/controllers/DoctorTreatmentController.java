package com.miniproject.mp301.controllers;

import com.miniproject.mp301.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class DoctorTreatmentController {

    @Autowired
    private UserService userServe;


    @RequestMapping("treatment")
    public String viewtreatmentPage(Model model, HttpSession s) {
        String name = (String) s.getAttribute("name");
        String role = (String) s.getAttribute("role");
        Long id = (Long) s.getAttribute("id");
        Long biodataId = (Long) s.getAttribute("biodataId");

        if (id != null) {
            model.addAttribute("name", name);
            model.addAttribute("role", role);
            model.addAttribute("id", id);
            model.addAttribute("biodataId", biodataId);
            return "tindakan/tindakan.html";
        } else {
            return "landingPage.html";
        }
        //return "tindakan/tindakan.html";
    }
}
