package com.miniproject.mp301.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.miniproject.mp301.models.M_User;

public interface UserRepository extends JpaRepository<M_User, Long> {
	@Query(value = "SELECT * FROM m_user WHERE email = :email AND password = :password LIMIT 1", nativeQuery = true)
	M_User login(String email, String password);

	@Query(value = "SELECT * FROM m_user WHERE email = :email LIMIT 1", nativeQuery = true)
	M_User searchEmail(String email);

	@Query(value = "SELECT * FROM m_user WHERE password = :password", nativeQuery = true)
	M_User searchPassword(String password);

	@Query(value = "UPDATE M_User SET last_login = :lastLogin WHERE email = :email", nativeQuery = true)
	@Modifying
	void updatelastLogin(Date lastLogin, String email);
	
	@Query(value = "UPDATE M_User SET login_attempt = :loginAttempt WHERE email = :email", nativeQuery = true)
	@Modifying
	void updateLoginAttempts(Integer loginAttempt, String email);
	
	@Query(value = "UPDATE M_User SET is_locked = :lock WHERE email = :email", nativeQuery = true)
	@Modifying
	void updateIsLock(Boolean lock, String email);
	
	
}
