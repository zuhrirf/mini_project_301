package com.miniproject.mp301.controllers;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.models.M_User;
import com.miniproject.mp301.services.UserService;

@RestController
@RequestMapping("api/login")
public class UserRestController {
	@Autowired
	private UserService userServe;

	@GetMapping("getAllUser")
	public List<M_User> getAllUser() {
		return userServe.getAllUser();
	}

	@GetMapping("getUser/{id}")
	public M_User getUserById(@PathVariable("id") Long id) {
		return userServe.getUserById(id);
	}

	@GetMapping("/emailAndPassword")
	public M_User searchEmailAndPassword(@RequestParam("email") String email,
			@RequestParam("password") String password) {
		M_User userLogin = userServe.login(email, password);
		M_User userByEmail = userServe.searchEmail(email);

		if (userLogin != null) {	
			userLogin.setLast_login(new Date());
			Date lastLogin = userLogin.getLast_login();
			userServe.updateLastLogin(lastLogin, email);
			userServe.updateLoginAttempt(0, email);
			return userLogin;
		} else {
			if (userByEmail != null) {

				Integer failAttempts = userByEmail.getLogin_attempt() + 1;
				userServe.updateLoginAttempt(failAttempts, email);
				if (userByEmail.getLogin_attempt() >= 3) {
					userByEmail.setIs_locked(true);
					Boolean isLock = userByEmail.getIs_locked();
					userServe.updateIsLock(isLock, email);
					return userByEmail;
				}

				return userLogin;

			} else {

				return userLogin;
			}
		}
	}

	@GetMapping("/email")
	public M_User getUserByEmail(@RequestParam("email") String email) {
		return userServe.searchEmail(email);
	}

	@GetMapping("/password")
	public M_User getUserByPassword(@RequestParam("password") String password) {
		return userServe.searchPassword(password);
	}
}
