package com.miniproject.mp301.controllers;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.miniproject.mp301.models.M_User;
import com.miniproject.mp301.services.UserService;

@Controller
public class DefaultController {
	@Autowired
	private UserService userServe;

	@RequestMapping("")
	public String viewLandingPage(HttpSession s) {
		return "landingPage.html";
	}

	@RequestMapping("/register")
	public String signup(HttpSession s) {
		return "landingPage.html";
	}

	@RequestMapping("/login")
	public String login(String email, String password, HttpSession s) {
		M_User user = userServe.login(email, password);
		if (user != null) {
			// Buat variabel global yang diisi user
			s.setAttribute("id", user.getId());
			s.setAttribute("email", email);
			s.setAttribute("role", user.getRole().getName());
			s.setAttribute("name", user.getBiodata().getFullname());
			s.setAttribute("biodataId", user.getBiodata().getId());
			s.setAttribute("roleId", user.getRole().getId());
//			System.out.println(email);
//			System.out.println(user.getBiodata().getFullname());
//			System.out.println(user.getRole().getName());
			return "menu.html";
		} else {
			return "landingPage.html";
		}
	}

	/*@RequestMapping("treatment")
	public String viewtreatmentPage(Model model, HttpSession s) {
		String name = (String) s.getAttribute("name");
		String role = (String) s.getAttribute("role");
		Long id = (Long) s.getAttribute("id");
		Long biodataId = (Long) s.getAttribute("biodataId");

		if (id != null) {
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			return "tindakan/tindakan.html";
		} else {
			return "landingPage.html";
		}

	//return "tindakan/tindakan.html";
	}*/

	/*@RequestMapping("profile")
	public String viewProfilePage(Model model, HttpSession s) {
		String name = (String) s.getAttribute("name");
		String role = (String) s.getAttribute("role");
		Long id = (Long) s.getAttribute("id");
		Long biodataId = (Long) s.getAttribute("biodataId");

		if (id != null) {
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			return "profile/profile.html";
		} else {
			return "landingPage.html";
		}

		//return "profile/profile.html";
	}*/

	@RequestMapping("menu")
	public String viewDashboard(Model model, HttpSession s) {
		String email = (String) s.getAttribute("email");
		String name = (String) s.getAttribute("name");
		String role = (String) s.getAttribute("role");
		Long id = (Long) s.getAttribute("id");
		Long biodataId = (Long) s.getAttribute("biodataId");
		Long roleId = (Long) s.getAttribute("roleId");

		if (email != null) {
			model.addAttribute("email", email);
			model.addAttribute("name", name);
			model.addAttribute("role", role);
			model.addAttribute("id", id);
			model.addAttribute("biodataId", biodataId);
			model.addAttribute("roleId", roleId);
			return "menu.html";
		} else {
			return "landingPage.html";
		}
	}

	@RequestMapping("/logout")
	public String logOut(HttpSession s) {
		// hancurkan session
		s.invalidate();
		return "landingPage.html";
	}
}
