package com.miniproject.mp301.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("api/image")
public class ImageRestController {
	public static String fileDownloadedPath = "C:\\Users\\XSIS\\Documents\\T-710\\DownloadedImage\\";

	@PostMapping("upload")
	public String uploadImg(@RequestParam("file") MultipartFile file) {
		// ambil path file yang akan diupload
		String fileOri = StringUtils.cleanPath(file.getOriginalFilename());
		// file nya ditaruh mana + namanya
		Path path = Paths.get(fileDownloadedPath + fileOri);

		// Copy file ke path yang diinginkan
		try {
			Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Membuat Uri atau samaran lokasi image ori
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/images/").path(fileOri)
				.toUriString();

		return fileDownloadUri;
	}
}