package com.miniproject.mp301.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.miniproject.mp301.models.M_Role;
import com.miniproject.mp301.services.RoleService;

@RestController
@RequestMapping("api/role")
public class RoleRestController {
	@Autowired
	private RoleService roleServe;
	
	@GetMapping("getAllRoles")
	public List<M_Role> getAllRoles() {
		return roleServe.getAllRoles();
	} 
}
