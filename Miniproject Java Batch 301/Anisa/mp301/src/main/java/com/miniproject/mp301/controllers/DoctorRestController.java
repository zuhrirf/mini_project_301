package com.miniproject.mp301.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.models.MDoctorModel;
import com.miniproject.mp301.services.DoctorService;

@RestController
@RequestMapping("api/doctor")
public class DoctorRestController {
	@Autowired
	private DoctorService doctorServe;

	@GetMapping("getDoctorByBiodataId/{biodataId}")
	public MDoctorModel getDoctorByBiodataId(@PathVariable("biodataId") Long biodata_id) {
		return doctorServe.getDoctorByBiodata_id(biodata_id);
	}
}
