$(document).ready(function() {
	// Eye Icon Lihat Password
	$("#show_hide_new_password_register a").on('click', function(event) {
		event.preventDefault();
		if ($('#show_hide_new_password_register input').attr("type") == "text") {
			$('#show_hide_new_password_register input').attr('type', 'password');
			$('#show_hide_new_password_register i').addClass("fa-eye-slash");
			$('#show_hide_new_password_register i').removeClass("fa-eye");
		} else if ($('#show_hide_new_password_register input').attr("type") == "password") {
			$('#show_hide_new_password_register input').attr('type', 'text');
			$('#show_hide_new_password_register i').removeClass("fa-eye-slash");
			$('#show_hide_new_password_register i').addClass("fa-eye");
		}
	});

	$("#show_hide_new_reenter_password_register a").on('click', function(event) {
		event.preventDefault();
		if ($('#show_hide_new_reenter_password_register input').attr("type") == "text") {
			$('#show_hide_new_reenter_password_register input').attr('type', 'password');
			$('#show_hide_new_reenter_password_register i').addClass("fa-eye-slash");
			$('#show_hide_new_reenter_password_register i').removeClass("fa-eye");
		} else if ($('#show_hide_new_reenter_password_register input').attr("type") == "password") {
			$('#show_hide_new_reenter_password_register input').attr('type', 'text');
			$('#show_hide_new_reenter_password_register i').removeClass("fa-eye-slash");
			$('#show_hide_new_reenter_password_register i').addClass("fa-eye");
		}
	});
});

// Generated OTP 6 digit
function generateOTP() {
	var digits = '0123456789';
	let OTP = '';
	for (let i = 0; i < 6; i++) {
		OTP += digits[Math.floor(Math.random() * 10)];
	}
	return OTP;
}


function getUserByEmail(email) {
	return $.ajax({
		url: "/api/login/email?email=" + email,
		method: "GET",
		async: false
	});
}

function sendEmail(myJson) {
	return $.ajax({
		url: "/api/token/sendMail",
		type: "POST",
		contentType: "application/json",
		data: myJson,
		async: false
	});
}
function addTokenByEmail(myJson) {
	return $.ajax({
		url: "/api/token/insertTokenResgistrasi",
		type: "POST",
		contentType: "application/json",
		data: myJson,
		async: false
	});
}

function getTokenByOTP(otp, email) {
	return $.ajax({
		url: "/api/token/getOtpByTokenAndEmail?token=" + otp + "&email=" + email,
		type: "GET",
		async: false
	});
}

function setExpiredTokenByEmail(email) {
	return $.ajax({
		url: "/api/token/setExpiredTokenAllEmail?email=" + email,
		type: "PUT",
		async: false
	});
}

function getRoleList() {
	return $.ajax({
		url: "/api/role/getAllRoles",
		type: "GET",
		async: false
	});
}

function addUserAndBio(myJson) {
	return $.ajax({
		url: "/api/register",
		type: "POST",
		contentType: "application/json",
		data: myJson,
		async: false
	});
}


// Register
$("#btnRegisterFirst").click(function() {
	$("#typeEmailRegister").val("");
	$("#errEmailRegister").text("");
	$("#registerModal").modal("show");
});


var spnRegister = document.getElementById("countRegister");
console.log(spnRegister);
var countRegister = 10;     // Set count
var timerRegister = null;  // For referencing the timer

var email;
// Click button sign up
$("#btnRegister").click(function() {
	email = $("#typeEmailRegister").val().trim();
	if (email == "") {
		$("#errEmailRegister").text("Email Must be fill!");
		return;
	} else {
		$("#errEmailRegister").text("");
	}

	var userByMail = getUserByEmail(email).responseJSON;
	if (userByMail != null) {
		//console.log(userByMail);
		$("#errEmailRegister").text("Email is Exist!");
	} else {
		var obj = {};
		obj.email = email;
		obj.token = generateOTP();

		var myJson = JSON.stringify(obj);
		console.log(myJson);
		var sendMail = sendEmail(myJson).responseText;
		//		console.log(sendMail);
		if (sendMail == "Successful") {
			alert("Successful Send Email");
			var addToken = addTokenByEmail(myJson).responseText;
			if (addToken == "ok") {
				$("#verifyModalRegister").modal("show");
				$("#registerModal").modal("hide");
				// Countdown Timer
				// Get refreence to span and button
				(function countDown() {
					// Display counter and start counting down
					spnRegister.textContent = countRegister;

					var minutes = parseInt(countRegister / 60, 10);
					var seconds = parseInt(countRegister % 60, 10);

					minutes = minutes < 10 ? "0" + minutes : minutes;
					seconds = seconds < 10 ? "0" + seconds : seconds;

					spnRegister.textContent = minutes + ":" + seconds;
					// Run the function again every second if the count is not zero
					if (countRegister !== 0) {
						timerRegister = setTimeout(countDown, 1000);
						countRegister--; // decrease the timer
					} else {
						$("#btnCounterRegister").css("color", "DodgerBlue");
						$("#btnCounterRegister").css("cursor", "pointer");

						// Enable the button, Button kirim ulang
						$("#btnCounterRegister").click(function() {
							setExpiredTokenByEmail(email);
							
							var resend = {};
							resend.email = email;
							resend.token = generateOTP();
							var resendJson = JSON.stringify(resend);
							sendMail = sendEmail(resendJson).responseText;
							console.log(resendJson);
							if (sendMail = "Successful") {
								alert("Successful send Email")
								addOtp = addTokenByEmail(resendJson).responseText;
							} else {
								alert(sendMail);
							}
						});
					}
				}());
			} else {
				alert(addToken);
			}
		} else {
			alert(sendMail);
		}
	}
});

// Click button confirm OTP
$("#btnConfirmOTPRegister").click(function() {
	var otp = $("#typeOTPRegister").val();
	//console.log(otp);
	if (otp == "") {
		$("#errOTPRegister").text("OTP Must be fill!");
		return;
	} else {
		$("#errOTPRegister").text("");
	}

	var getOTP = getTokenByOTP(otp, email).responseJSON;
	if (getOTP != null) {
		//console.log(getOTP.token)
		if (getOTP.is_expired == true) {
			alert("Please resend OTP, because your time was out")
			$("#typeEmailRegister").val("");
		} else if (getOTP.is_expired == false) {
			$("#errOTPRegister").text("");
			$("#typeOTPRegister").val("");
			$("#setPasswordModalRegister").modal("show");
			$("#verifyModalRegister").modal("hide");
		}
	} else {
		$("#errOTPRegister").text("Invalid OTP");
	}
});

var newPasswd;
var newRePasswd;
// Click button set password
$("#btnSetPasswordRegister").click(function() {
	newPasswd = $("#typePasswordResetRegister").val().trim();
	newRePasswd = $("#typeRePasswordResetRegister").val().trim();
	const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/;
	if (newRePasswd == "" || newPasswd == "") {
		$("#errRePasswdResetRegister").text("Paswword must be fill!");
		//console.log("kosong", newPasswd, newRePasswd);
	} else if (newPasswd != newRePasswd) {
		$("#errRePasswdResetRegister").text("Passwword don't match!");
		//console.log(newPasswd, newRePasswd);
	} else if (!newPasswd.match(pattern) && !newRePasswd.match(pattern)) {
		//alert("salah");
		$("#errRePasswdResetRegister").text("Please fill with request format");
	} else {
		$("#errRePasswdResetRegister").text("");
		$("#daftarModal").modal("show");
		$("#setPasswordModalRegister").modal("hide");

		getRoleDropdown();
	}
});


function getRoleDropdown() {
	var role = getRoleList().responseJSON;
	if (role != null) {
		//		console.log(role)
		for (i = 1; i < role.length - 1; i++) {
			$("#selectRoleName").append(
				`
						<option value="${role[i].id}">${role[i].name}</option> 	
					`
			)
		}
	}
}


var fullname;
var phoneNumber;
var rolename;
// Click btn daftar
$("#btnDaftar").click(function() {
	var user = {};
	user.email = email;
	user.password = newPasswd;
	user.role_id = $("#selectRoleName").val();

	var bio = {};
	bio.fullname = $("#typeFullname").val();
	bio.mobile_phone = "+62 " + $("#typePhoneNumber").val();

	var obj = { user, bio };
	var myJson = JSON.stringify(obj);
	//	console.log(myJson);

	var resposeInsertUserAndBio = addUserAndBio(myJson).responseText
	if (resposeInsertUserAndBio == "ok") {
		$("#successModal").modal("show");
		$("#daftarModal").modal("hide");
	} else {
		alert(resposeInsertUserAndBio);
	}
});
// Register End

