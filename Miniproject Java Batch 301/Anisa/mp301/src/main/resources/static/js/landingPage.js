$(document).ready(function() {

	showPublicMenus();
});

// Menu
function getMenusByRoleId(roleId) {
	return $.ajax({
		url: "/api/menu/getMenusByRoleId/" + roleId,
		method: "GET",
		async: false
	});
}

function showPublicMenus() {
	var menus = getMenusByRoleId(4).responseJSON;
	//console.log(menus);
	$("#Menus").html("");
	for (i = 0; i < menus.length; i++) {
		$("#Menus").append(`
			<div>
				<img src="${menus[i].menu.big_icon}" width="180px">
				<p>${menus[i].menu.name}</p>
			</div>
		`)
	}
}
// Menu End