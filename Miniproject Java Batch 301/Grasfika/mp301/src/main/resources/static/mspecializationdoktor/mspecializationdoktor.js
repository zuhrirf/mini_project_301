function getAllMSpecializationDoktors(){
	$("#msdTable").html(
		`<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="msdTBody"></tbody>
		`
	);
	
	$.ajax({
		url : "/api/getallmspecializationdoktors",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#msdTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
						<td>
							<i 
							${data[i].active ? `class="bi-check-circle-fill" style="color:green"` : `class="bi-x-circle-fill" style="color:red"`}>
						</td>
						<td>
							<button value="${data[i].id}" onClick="editMSD(this.value)" class="btn btn-warning">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deleteMSD(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					
					`
					
				)				
			}
		}
	});	
}

$("#addBtn").click(function(){
	$.ajax({
		url: "/m_specialization/addm_specialization",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Create New Specialization Doctor");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})


function editMSD(id){
	$.ajax({
		url: "/m_specialization/m_specialization/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Edit Specialization Doctor");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deleteMSD(id){
	$.ajax({
		url: "/m_specialization/deletem_specialization/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("Delete Specialization Doctor");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

$(document).ready(function(){
	getAllMSpecializationDoktors();
	
})
