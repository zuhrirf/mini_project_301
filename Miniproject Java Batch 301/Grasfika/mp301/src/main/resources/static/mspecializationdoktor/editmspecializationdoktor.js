$(document).ready(function() {
	getMSpecializationDoktorById();

})

function getMSpecializationDoktorById() {
	var id = $("#editMSDId").val();
	$.ajax({
		url: "/api/getm_specialization/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameEditInput").val(data.name);
			if (data.active) { $("#activeEditInput").prop("checked", true); }
		}
	})
}

$("#editMSDBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editMSDBtnCreate").click(function() {
	var id = $("#editMSDId").val();
	var name = $("#nameEditInput").val();
	var active = $("#activeEditInput").is(":checked") ? true : false;

	if (name == "") {
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	var obj = {};
	obj.id = id;
	obj.name = name;
	obj.active = active;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editm_specialization",
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
			if (data == "ok") {
				$(".modal").modal("hide")
				getAllMSpecializationDoktors();
			}  else {
				$("#errName").text(data);
			}

		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})