$("#addMSDBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addMSDBtnCreate").click(function(){
	var name = $("#nameInput").val();
	var active = $("#activeInput").is(":checked") ? true : false;

	if(name == ""){
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}
	
	var obj = {};
	obj.name = name;
	obj.active = active;
	
	var myJson = JSON.stringify(obj);
	
	$.ajax({
		url : "/api/addm_specialization",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
			if(data == "ok"){
				$(".modal").modal("hide")
			getAllMSpecializationDoktors();
			} else {
				$("#errName").text(data);
			}
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})