$(document).ready(function() {
	getMSpecializationDoktorById();

})

function getMSpecializationDoktorById() {
	var id = $("#delMSDId").val();
	$.ajax({
		url: "/api/getm_specialization/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			$("#nameDel").text(data.name);
			if (data.active) { $("#activeDel").prop("checked", true); }
		}
	})
}

$("#delMSDBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delMSDBtnDelete").click(function() {
	var id = $("#delMSDId").val();
	$.ajax({
		url : "/api/deletem_specialization/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
			if(data == "ok"){
				$(".modal").modal("hide")
				getAllMSpecializationDoktors();
			}
			
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})