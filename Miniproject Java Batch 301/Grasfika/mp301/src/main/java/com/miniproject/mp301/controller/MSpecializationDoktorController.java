package com.miniproject.mp301.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("m_specialization")
public class MSpecializationDoktorController {

        @RequestMapping("")
        public String MSpecializationDoktor() {
            return "m_specialization/m_specialization";
        }
        @RequestMapping("addm_specialization")
        public String addMSpecializationDoktor() {
            return "m_specialization/addm_specialization";
        }
        @RequestMapping("editm_specialization/{id}")
        public String editMSpecializationDoktor(@PathVariable("id") Long id, Model model) {
            model.addAttribute("id", id);
            return "m_specialization/editm_specialization";
        }
        @RequestMapping("deletem_specialization/{id}")
        public String deleteMSpecializationDoktor(@PathVariable("id") Long id, Model model) {
            model.addAttribute("id", id);
            return "m_specialization/deletem_specialization";
        }
    }
