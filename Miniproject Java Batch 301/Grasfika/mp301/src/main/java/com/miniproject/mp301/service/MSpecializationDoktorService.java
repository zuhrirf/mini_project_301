package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MSpecializationDoktorModel;
import com.miniproject.mp301.repository.MSpecializationDoktorRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import java.util.List;

@Transactional
public class MSpecializationDoktorService implements MSpecializationDoktorServiceImplements {

    private MSpecializationDoktorRepository MSpecializationDoktorRepo;

    public MSpecializationDoktorService(MSpecializationDoktorRepository sr) {
        this.MSpecializationDoktorRepo = sr;
    }

    @Override
    public List<MSpecializationDoktorModel> getAllMSpecializationDoktors(){
        return MSpecializationDoktorRepo.findAll();
    }
    @Override
    public MSpecializationDoktorModel getMSpecializationDoktorById(Long id) {
        return MSpecializationDoktorRepo.findById(id).get();
    }
    @Override
    public List<MSpecializationDoktorModel> cariName(String cari){
        return MSpecializationDoktorRepo.cariName(cari);
    }
    @Override
    public List<MSpecializationDoktorModel> searchMSpecializationDoktor(@Param("textsearch") String textsearch){
        return MSpecializationDoktorRepo.searchMSpecializationDoktor(textsearch);
    }
    @Override
    public void addMSpecializationDoktor(MSpecializationDoktorModel msd) {
        MSpecializationDoktorRepo.save(msd);
    }

    @Override
    public void editMSpecializationDoktor(MSpecializationDoktorModel msd) {
        MSpecializationDoktorRepo.save(msd);
    }
    @Override
    public void deleteMSpecializationDoktor(MSpecializationDoktorModel id) {
        MSpecializationDoktorRepo.save(id);
    }

}

