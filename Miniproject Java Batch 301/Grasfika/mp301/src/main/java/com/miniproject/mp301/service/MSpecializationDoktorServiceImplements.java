package com.miniproject.mp301.service;

import com.miniproject.mp301.model.MSpecializationDoktorModel;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MSpecializationDoktorServiceImplements { public List<MSpecializationDoktorModel> getAllMSpecializationDoktors();
        public MSpecializationDoktorModel getMSpecializationDoktorById(Long id);
        public List<MSpecializationDoktorModel> cariName(String cari);
        public List<MSpecializationDoktorModel> searchMSpecializationDoktor(@Param("textsearch") String textsearch);
        public void addMSpecializationDoktor(MSpecializationDoktorModel ms);
        public void editMSpecializationDoktor(MSpecializationDoktorModel ms);
        void deleteMSpecializationDoktor(MSpecializationDoktorModel id);

    }


