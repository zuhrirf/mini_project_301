package com.miniproject.mp301.controller;

import com.miniproject.mp301.model.MSpecializationDoktorModel;
import com.miniproject.mp301.service.MSpecializationDoktorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api")
public class MSpecializationDoktorRestController {

        @Autowired
        private MSpecializationDoktorService MSpecializationDoktorServe;

        @GetMapping("getallmspecializationdoktors")
        public List<MSpecializationDoktorModel> getAllMSpecializationDoktors() {
            List<MSpecializationDoktorModel> specializations = MSpecializationDoktorServe.getAllMSpecializationDoktors();
            List<MSpecializationDoktorModel> sortedData = specializations.stream()
                    .sorted((id1, id2) -> id1.getName().compareTo(id2.getName()))
                    .filter(data -> data.getIs_delete()== false)
                    .collect(Collectors.toList());

            return sortedData;
        }

        @GetMapping("getm_specialization/{id}")
        public MSpecializationDoktorModel getMSpecializationDoktorById(@PathVariable("id") Long id) {
            return MSpecializationDoktorServe.getMSpecializationDoktorById(id);
        }

    @GetMapping("getm_specialization/search")
    public ResponseEntity<List<MSpecializationDoktorModel>> getAllMSpecializationDoktors(@RequestParam("textsearch") String textsearch){
        try {
            List<MSpecializationDoktorModel> m_specialization = this.MSpecializationDoktorServe.searchMSpecializationDoktor(textsearch);
            return new ResponseEntity<>(m_specialization, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

    }

        @PostMapping("addm_specialization")
        public String AddM_Specialization(@RequestBody MSpecializationDoktorModel msd) {
            List<MSpecializationDoktorModel> listNameMSD = MSpecializationDoktorServe.cariName(msd.getName());
            if (listNameMSD.size()>0) {
                return "Spesialisasi Sudah Dipakai!";
            }
            //msd.setCreated_by(1L);
            //msd.setCreate_on(new Date());
            MSpecializationDoktorServe.addMSpecializationDoktor(msd);
            return "ok";
        }


        @PutMapping("editm_specialization")
        public String editM_Specialization(@RequestBody MSpecializationDoktorModel msd) {
            MSpecializationDoktorModel msd0 = MSpecializationDoktorServe.getMSpecializationDoktorById(msd.getId());
            List<MSpecializationDoktorModel> listNameMSD = MSpecializationDoktorServe.cariName(msd.getName());
            if (listNameMSD.size()>0 && !msd.getName().equals(msd0.getName())) {
                return "Spesialisasi Sudah Dipakai!";
            }

            msd.setCreated_by(msd0.getCreated_by());
            msd.setCreate_on(msd0.getCreate_on());
            msd.setModified_by(1L);
            msd.setModified_on(new Date());
            MSpecializationDoktorServe.editMSpecializationDoktor(msd);
            return "ok";
        }

        @DeleteMapping("deletem_specialization/{id}")
        public String deleteM_Specialization(@PathVariable("id") MSpecializationDoktorModel msd) {
            MSpecializationDoktorModel msd0 = MSpecializationDoktorServe.getMSpecializationDoktorById(msd.getId());

            msd0.setDelete_by(1L);
            msd0.setDelete_on(new Date());
            msd0.setIs_delete(true);
            MSpecializationDoktorServe.deleteMSpecializationDoktor(msd0);
            return "ok";
        }
    }


