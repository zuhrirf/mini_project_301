package com.miniproject.mp301.model;

import jakarta.persistence.*;

@Entity
@Table(name = "m_specialization")
public class MSpecializationDoktorModel extends BaseProperties {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 50)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
