package com.miniproject.mp301.repository;

import com.miniproject.mp301.model.MSpecializationDoktorModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MSpecializationDoktorRepository extends JpaRepository<MSpecializationDoktorModel, Long> {

    @Query(value = "SELECT * FROM m_specialization WHERE name = :cari", nativeQuery = true)
    List<MSpecializationDoktorModel> cariName(String cari);

    @Query(value="SELECT * FROM m_specialization ms WHERE ms.name ILIKE %:textsearch% AND is_deleted is false", nativeQuery=true)
    List<MSpecializationDoktorModel> searchMSpecializationDoktor(@Param("textsearch") String textsearch);



}
