$(document).ready(function() {

	showPublicMenu();
});

// Menu
function getMenuByRoleId(roleId) {
	return $.ajax({
		url: "/api/menu/getmenubyroleid/" + roleId,
		method: "GET",
		async: false
	});
}

function showPublicMenu() {
	var menus = getMenuByRoleId(4).responseJSON;
	$("#Menus").html("");
	for (i = 0; i < menus.length; i++) {
		$("#Menus").append(`
			<div>
				<img src="${menus[i].menu.big_icon}" width="180px">
				<p>${menus[i].menu.name}</p>
			</div>
		`)
	}
}
// Menu End