-- Isi data User
INSERT INTO m_user 
(biodata_id, role_id, email, password, login_attempt, is_locked, last_login, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted) 
VALUES
(null, null, null, null, null, false, null, 1, now(), null, null, null, false),
(null, null, 'Admin', 'Admin', null, false, null, 2, now(), null, null, null, false), 
(null, null, 'afifraihan33@gmail.com', '123456', 0, false, null, 3, now(), null, null, null, false),
(null, null, 'oktianto99@gmail.com', '123', 0, false, null, 4, now(), null, null, null, false),
(null, null, 'zuhrirf@gmail.com', 'abc', 0, false, null, 5, now(), null, null, null, false);

-- Isi Data Biodata
INSERT INTO m_biodata 
(fullname, mobile_phone, image, image_path, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted) 
VALUES 
('Admin', null, null, null, 1, now(), null, null, null, false),
('Ghost Rider', null, null, null, 1, now(), null, null, null, false),
('Taufik Nur', null, null, null, 1, now(), null, null, null, false),
('Mahdy', null, null, null, 1, now(), null, null, null, false);

-- isi Data Role
INSERT INTO m_role 
(name, code, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted) 
VALUES 
('Admin', 'ADM', 1, now(), null, null, null, false),
('Dokter', 'DOK', 1, now(), null, null, null, false),
('Pasien', 'PSN', 1, now(), null, null, null, false),
('Public', 'PUB', 1, now(), null, null, null, false);

--  Update data user
UPDATE m_user
SET biodata_id = 1, role_id = 1 
WHERE id = 2;

UPDATE m_user
SET biodata_id = 2, role_id = 2 
WHERE id = 3;

UPDATE m_user
SET biodata_id = 3, role_id = 3 
WHERE id = 4;

UPDATE m_user
SET biodata_id = 4, role_id = 2
WHERE id = 5;

-- Isi data doctor

-- Isi data menu
INSERT INTO m_menu
(name, url, parent_id, big_icon, small_icon, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted)
VALUES
-- PublicMenu
('Public Menu 1', null, null, 'http://localhost/images/big-menu.svg', null, 1, now(), null, null, null, false),
('Public Menu 2', null, null, 'http://localhost/images/big-menu.svg', null, 1, now(), null, null, null, false),
('Public Menu 3', null, null, 'http://localhost/images/big-menu.svg', null, 1, now(), null, null, null, false),
('Public Menu 4', null, null, 'http://localhost/images/big-menu.svg', null, 1, now(), null, null, null, false),
('Public Menu 5', null, null, 'http://localhost/images/big-menu.svg', null, 1, now(), null, null, null, false),
-- AdminMenu
('Admin Menu 1', null, null, 'http://localhost/images/big-menu.svg', 'http://localhost/images/icon-medic.png', 1, now(), null, null, null, false),
('Admin Menu 2', null, null, 'http://localhost/images/big-menu.svg', 'http://localhost/images/icon-medic.png', 1, now(), null, null, null, false),
('Admin Menu 3', null, null, 'http://localhost/images/big-menu.svg', 'http://localhost/images/icon-shop.png', 1, now(), null, null, null, false),
-- PasienMenu
--9
 ('Pasien Main Menu 1',  null, null, null, 'http://localhost/images/icon-medic.png', 1, now(), null, null, null, false),
('Pasien Sub Menu 1',   null, 9, 'http://localhost/images/pasien-menu.svg', null , 1, now(), null, null, null, false),
--11
('Pasien Sub Menu 2',   null, 9, 'http://localhost/images/pasien-menu.svg', null, 1, now(), null, null, null, false),
('Pasien Main Menu 2',  null, null, null, 'http://localhost/images/icon-shop.png', 1, now(), null, null, null, false),
('Pasien Sub Menu 3',   null, 12, 'http://localhost/images/pasien-menu2.svg', null, 1, now(), null, null, null, false),
('Pasien Main Menu 3',  null, null, null, 'http://localhost/images/icon-shop.png', 1, now(), null, null, null, false),
-- DoktorMenu
('Dokter Main Menu 1',  null, null, null, 'http://localhost/images/icon-medic.png', 1, now(), null, null, null, false),
('Dokter Sub Menu 1',   null, 15, 'http://localhost/images/dokter-menu.svg', null, 1, now(), null, null, null, false),
('Dokter Main Menu 2',  null, null, null, 'http://localhost/images/icon-shop.png', 1, now(), null, null, null, false),
('Dokter Sub Menu 2',   null, 17, 'http://localhost/images/dokter-menu2.svg', null, 1, now(), null, null, null, false);

-- Isi menu role
INSERT INTO m_menu_role
(menu_id, role_id, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted)
VALUES
-- Public Menu
(1, 4, 1, now(), null, null, null, false),
(2, 4, 1, now(), null, null, null, false),
(3, 4, 1, now(), null, null, null, false),
(4, 4, 1, now(), null, null, null, false),
(5, 4, 1, now(), null, null, null, false),
-- AdminMenu
(6, 1, 1, now(), null, null, null, false),
(7, 1, 1, now(), null, null, null, false),
(8, 1, 1, now(), null, null, null, false),
-- PasienMenu
(9, 3, 1, now(), null, null, null, false),
(10, 3, 1, now(), null, null, null, false),
(11, 3, 1, now(), null, null, null, false),
(12, 3, 1, now(), null, null, null, false),
(13, 3, 1, now(), null, null, null, false),
(14, 3, 1, now(), null, null, null, false),

-- DokterMenu
(15, 2, 1, now(), null, null, null, false),
(16, 2, 1, now(), null, null, null, false),
(17, 2, 1, now(), null, null, null, false),
(18, 2, 1, now(), null, null, null, false);



-- INSERT INTO t_doctor_treatment
-- (doctor_id, name, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted)
-- VALUES
-- (null, 'Vaksin anak', 1, now(), null, null, null, false);

-- INSERT INTO t_token 
-- (email, user_id, token, expired_on, is_expired, used_for, created_by, created_on, modified_by, modified_on, deleted_on, is_deleted) 
-- VALUES 
-- ('cursedemonic@gmail.com', 1, '246802', null, false, null, 1, now(), null, null, null, false);



