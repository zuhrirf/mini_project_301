package com.miniproject.mp301.repositories;

import com.miniproject.mp301.model.TResetPasswordModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TResetPasswordRepository extends JpaRepository<TResetPasswordModel,Long> {
}
