package com.miniproject.mp301.request;


import com.miniproject.mp301.model.MUserModel;
import com.miniproject.mp301.model.TResetPasswordModel;

public class insertNewPasswordAndEditUserPassword {
    private MUserModel user;
    private TResetPasswordModel resetPassword;

    public MUserModel getUser() {
        return user;
    }

    public void setUser(MUserModel user) {
        this.user = user;
    }

    public TResetPasswordModel getResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(TResetPasswordModel resetPassword) {
        this.resetPassword = resetPassword;
    }
}