package com.miniproject.mp301.service;

import com.miniproject.mp301.model.TTokenModel;
import com.miniproject.mp301.repositories.TTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TTokenService {
    @Autowired
    private TTokenRepository tokenRepo;

    public List<TTokenModel> getAllToken() {
        return tokenRepo.findAll();
    }

    public void insertToken(TTokenModel tokenModel) {
        tokenRepo.save(tokenModel);
    }

    public TTokenModel searchOTP(String token, String email) {
        return tokenRepo.searchOTP(token, email);
    }

    public void updateIsExpired(Boolean expired, String token) {
        tokenRepo.updateIsExpired(expired, token);
    }

    public void updateIsExpiredAllEmail(Boolean expired, String email) {
        tokenRepo.updateIsExpiredAllEmail(expired, email);
    }
}
