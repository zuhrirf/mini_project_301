package com.miniproject.mp301.controller;

import com.miniproject.mp301.model.MUserModel;
import com.miniproject.mp301.model.TResetPasswordModel;
import com.miniproject.mp301.request.insertNewPasswordAndEditUserPassword;
import com.miniproject.mp301.service.MUserService;
import com.miniproject.mp301.service.TResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("api/reset")
public class TResetPasswordRestController {
    @Autowired
    private MUserService userServe;
    @Autowired
    private TResetPasswordService resetServe;

    @PostMapping("resetPassword")
    public String insertRePasswd(@RequestBody insertNewPasswordAndEditUserPassword request) {
        MUserModel userLama = userServe.searchEmail(request.getUser().getEmail());

//		String emailLama = userLama.getEmail();
        String passwordLama = userLama.getPassword();

        request.getResetPassword().setOld_password(passwordLama);
        request.getResetPassword().setCreated_by(userLama.getId());
        request.getResetPassword().setCreated_on(new Date());
        request.getResetPassword().setReset_for("Change Passsword");
        System.out.println(passwordLama);
        System.out.println(request.getResetPassword().getNew_password());
        if (!passwordLama.equals(request.getResetPassword().getNew_password())) {
            TResetPasswordModel resetPassword = resetServe.insertRePasswd(request.getResetPassword());
            request.getUser().setPassword(resetPassword.getNew_password());

//			request.getUser().setCreate_by(2L);
//			request.getUser().setCreate_on(new Date());
            userLama.setModified_on(new Date());
            userLama.setModified_by(userLama.getId());
            userLama.setIs_locked(false);
            userLama.setLogin_attempt(0);
            userLama.setPassword(request.getResetPassword().getNew_password());
            userServe.editUser(userLama);
            return "ok";
        } else {
            return "fail";
        }
    }
}
