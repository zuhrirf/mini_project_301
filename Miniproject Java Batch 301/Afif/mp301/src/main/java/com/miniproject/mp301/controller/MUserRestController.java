package com.miniproject.mp301.controller;

import java.util.Date;
import java.util.List;

import com.miniproject.mp301.model.MUserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.service.MUserService;

@RestController
@RequestMapping("api/login")
public class MUserRestController {
	@Autowired
	private MUserService mUserService;

	@GetMapping("getAllUser")
	public List<MUserModel> getAllUser() {
		return mUserService.getAllUser();
	}

	@GetMapping("getUser/{id}")
	public MUserModel getUserById(@PathVariable("id") Long id) {
		return mUserService.getUserById(id);
	}

	@GetMapping("/emailAndPassword")
	public MUserModel searchEmailAndPassword(@RequestParam("email") String email,
                                             @RequestParam("password") String password) {
		MUserModel userLogin = mUserService.login(email, password);
		MUserModel userByEmail = mUserService.searchEmail(email);

		if (userLogin != null) {	
			userLogin.setLast_login(new Date());
			Date lastLogin = userLogin.getLast_login();
			mUserService.updateLastLogin(lastLogin, email);
			mUserService.updateLoginAttempt(0, email);
			return userLogin;
		} else {
			if (userByEmail != null) {

				Integer failAttempts = userByEmail.getLogin_attempt() + 1;
				mUserService.updateLoginAttempt(failAttempts, email);
				if (userByEmail.getLogin_attempt() >= 3) {
					userByEmail.setIs_locked(true);
					Boolean isLock = userByEmail.getIs_locked();
					mUserService.updateIsLock(isLock, email);
					return userByEmail;
				}

				return userLogin;

			} else {

				return userLogin;
			}
		}
	}

	@GetMapping("/email")
	public MUserModel getUserByEmail(@RequestParam("email") String email) {
		return mUserService.searchEmail(email);
	}

	@GetMapping("/password")
	public MUserModel getUserByPassword(@RequestParam("password") String password) {
		return mUserService.searchPassword(password);
	}
}
