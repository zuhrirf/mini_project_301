package com.miniproject.mp301.model;

import javax.persistence.*;

@Entity
@Table(name = "t_reset_password")
public class TResetPasswordModel extends BasePropertiesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(length = 255)
    private String old_password;

    @Column(length = 255)
    private String new_password;

    @Column(length = 20)
    private String reset_for;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getReset_for() {
        return reset_for;
    }

    public void setReset_for(String reset_for) {
        this.reset_for = reset_for;
    }
}
