package com.miniproject.mp301.controller;

import com.miniproject.mp301.model.MUserModel;
import com.miniproject.mp301.model.TTokenModel;
import com.miniproject.mp301.service.MUserService;
import com.miniproject.mp301.service.TTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

public class TTokenRestController {
    @Autowired
    private MUserService userServe;

    @Autowired
    private TTokenService tokenServe;

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String sender;

//	private String otp = new DecimalFormat("000000").format(new Random().nextInt(999999));

    @PostMapping("sendMail")
    public String sendMail(@RequestBody TTokenModel tokenModel) {
        try {
            SimpleMailMessage smm = new SimpleMailMessage();

            smm.setFrom(sender);
            smm.setSubject("Kode OTP");
            smm.setTo(tokenModel.getEmail());
            smm.setText("Token Anda adalah " + tokenModel.getToken());

            // Kirim email
            javaMailSender.send(smm);
            return "Successful";
        } catch (Exception e) {
//			e.printStackTrace();
            return "Failed send email";
        }
    }

    @PostMapping("insertToken")
    public String insertToken(@RequestBody TTokenModel tokenModel) {
        long otpValidDuration = 30 * 1000;
        Date date = new Date();
        date.setTime(otpValidDuration + System.currentTimeMillis());

        MUserModel userByEmail = userServe.searchEmail(tokenModel.getEmail());
        tokenModel.setUser_id(userByEmail.getId());

        tokenModel.setCreated_by(userByEmail.getId());
        tokenModel.setCreated_on(new Date());
        tokenModel.setExpired_on(date);
        tokenModel.setIs_expired(false);
        tokenModel.setUsed_for("forget password");
        tokenServe.insertToken(tokenModel);
        return "ok";
    }

    @PostMapping("insertTokenResgistrasi")
    public String insertTokenRegistrasi(@RequestBody TTokenModel tokenModel) {
        long otpValidDuration = 2 * 60 * 1000;
        Date date = new Date();
        date.setTime(otpValidDuration + System.currentTimeMillis());

        tokenModel.setCreated_by(1L);
        tokenModel.setCreated_on(new Date());
        tokenModel.setExpired_on(date);
        tokenModel.setIs_expired(false);
        tokenModel.setUsed_for("registrasi");
        tokenServe.insertToken(tokenModel);
        return "ok";
    }

    @GetMapping("getAllToken")
    public List<TTokenModel> getAllToken() {
        return tokenServe.getAllToken();
    }

    @GetMapping("/getOtpByTokenAndEmail")
    public TTokenModel getOtpByToken(@RequestParam("token") String token, @RequestParam("email") String email) {
        TTokenModel tokenOtp = tokenServe.searchOTP(token, email);

        if (tokenOtp != null) {
            // Waktu sekarang
            long currentTimeInMillis = System.currentTimeMillis();

            // Waktu sekarang + durasi
            long otpExpiredInMillis = tokenOtp.getExpired_on().getTime();
//			System.out.println(currentTimeInMillis);
//			System.out.println(otpExpiredInMillis);

            if (otpExpiredInMillis < currentTimeInMillis) {
                tokenOtp.setIs_expired(true);
                tokenServe.updateIsExpired(tokenOtp.getIs_expired(), token);
                return tokenOtp;
            }
            return tokenOtp;
        } else {
            return tokenOtp;
        }
    }

    @PutMapping("setExpiredTokenAllEmail")
    public String setExpiredTokenAllEmail(@RequestParam("email") String email) {
        tokenServe.updateIsExpiredAllEmail(true, email);
        return "ok";
    }
}
