package com.miniproject.mp301.controller;

import java.util.List;

import com.miniproject.mp301.model.MRoleModel;
import com.miniproject.mp301.service.MRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/role")
public class MRoleRestController {
	@Autowired
	private MRoleService mRoleService;
	
	@GetMapping("getallrole")
	public List<MRoleModel> getAllRole() {
		return mRoleService.getAllRole();
	} 
}
