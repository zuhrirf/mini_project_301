package com.miniproject.mp301.service;

import java.util.List;

import com.miniproject.mp301.model.MMenuRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.miniproject.mp301.repositories.MMenuRoleRepository;

@Service
@Transactional
public class MMenuRoleService {
	@Autowired
	private MMenuRoleRepository mMenuRoleRepository;
	
	public List<MMenuRole> getMenuByRoleId(Long role_id) {
		return mMenuRoleRepository.getMenuByRoleId(role_id);
	}
	
	public List<MMenuRole> getMainMenuByRoleId(Long role_id){
		return mMenuRoleRepository.getMainMenuByRoleId(role_id);
	}
	
	public List<MMenuRole> getSubMenuByRoleId(Long role_id, Long parent_id){return mMenuRoleRepository.getSubMenuByRoleId(role_id, parent_id);}
}
