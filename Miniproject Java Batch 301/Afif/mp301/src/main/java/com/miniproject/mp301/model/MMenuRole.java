package com.miniproject.mp301.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_menu_role")
public class MMenuRole extends BasePropertiesModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "menu_id", insertable = false, updatable = false)
	private MMenuModel menu;

	private Long menu_id;

	@OneToOne
	@JoinColumn(name = "role_id", insertable = false, updatable = false)
	private MRoleModel role;

	private Long role_id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MMenuModel getMenu() {
		return menu;
	}

	public void setMenu(MMenuModel menu) {
		this.menu = menu;
	}

	public Long getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(Long menu_id) {
		this.menu_id = menu_id;
	}

	public MRoleModel getRole() {
		return role;
	}

	public void setRole(MRoleModel role) {
		this.role = role;
	}

	public Long getRole_id() {
		return role_id;
	}

	public void setRole_id(Long role_id) {
		this.role_id = role_id;
	}
}
