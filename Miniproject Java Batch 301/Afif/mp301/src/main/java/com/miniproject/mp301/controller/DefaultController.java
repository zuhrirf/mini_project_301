package com.miniproject.mp301.controller;

import javax.servlet.http.HttpSession;

import com.miniproject.mp301.model.MUserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.miniproject.mp301.service.MUserService;

@Controller
public class DefaultController {
	@Autowired
	private MUserService mUserService;

	@RequestMapping("")
	public String ViewLandingPage(HttpSession session){return "landingPage.html";}

	@RequestMapping("/register")
	public String signup(HttpSession s) {
		return "landingPage.html";
	}

	@RequestMapping("/login")
	public String login(String email, String password, HttpSession s) {
		MUserModel user = mUserService.login(email, password);
		if (user != null) {
			// Buat variabel global yang diisi user
			s.setAttribute("id", user.getId());
			s.setAttribute("email", email);
			s.setAttribute("role", user.getRole().getName());
			s.setAttribute("name", user.getBiodata().getFullname());
			s.setAttribute("biodataId", user.getBiodata().getId());
			s.setAttribute("roleId", user.getRole().getId());
//			System.out.println(email);
//			System.out.println(user.getBiodata().getFullname());
//			System.out.println(user.getRole().getName());
			return "menu.html";
		} else {
			return "landingPage.html";
		}
	}
}