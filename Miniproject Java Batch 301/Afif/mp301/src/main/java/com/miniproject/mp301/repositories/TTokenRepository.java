package com.miniproject.mp301.repositories;

import com.miniproject.mp301.model.TTokenModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface TTokenRepository extends JpaRepository<TTokenModel, Long> {
    @Query(value = "SELECT * FROM t_token WHERE token = :token  And email = :email LIMIT 1", nativeQuery = true)
    TTokenModel searchOTP(String token, String email);

    @Query(value = "UPDATE t_Token SET is_expired = :expired WHERE token = :token", nativeQuery = true)
    @Modifying
    void updateIsExpired(Boolean expired, String token);

    @Query(value = "UPDATE t_Token SET is_expired = :expired WHERE email = :email", nativeQuery = true)
    @Modifying
    void updateIsExpiredAllEmail(Boolean expired, String email);
}
