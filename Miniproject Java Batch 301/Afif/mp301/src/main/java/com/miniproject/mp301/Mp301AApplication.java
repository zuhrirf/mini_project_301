package com.miniproject.mp301;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mp301AApplication {
	public static void main(String[] args) {SpringApplication.run(Mp301AApplication.class, args);}
}