package com.miniproject.mp301.service;


import com.miniproject.mp301.model.TResetPasswordModel;
import com.miniproject.mp301.repositories.TResetPasswordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class TResetPasswordService {
    @Autowired
    private TResetPasswordRepository resetRepo;

    public TResetPasswordModel insertRePasswd(TResetPasswordModel resetModel) {
        return resetRepo.save(resetModel);}
}
