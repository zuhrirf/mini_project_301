package com.miniproject.mp301.repositories;

import java.util.Date;

import com.miniproject.mp301.model.MUserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MUserRepository extends JpaRepository<MUserModel, Long> {
	@Query(value = "SELECT * FROM m_user WHERE email = :email AND password = :password LIMIT 1", nativeQuery = true)
    MUserModel login(String email, String password);

	@Query(value = "SELECT * FROM m_user WHERE email = :email LIMIT 1", nativeQuery = true)
    MUserModel searchEmail(String email);

	@Query(value = "SELECT * FROM m_user WHERE password = :password", nativeQuery = true)
    MUserModel searchPassword(String password);

	@Query(value = "UPDATE m_user SET last_login = :lastLogin WHERE email = :email", nativeQuery = true)
	@Modifying
	void updatelastLogin(Date lastLogin, String email);
	
	@Query(value = "UPDATE m_user SET login_attempt = :loginAttempt WHERE email = :email", nativeQuery = true)
	@Modifying
	void updateLoginAttempts(Integer loginAttempt, String email);
	
	@Query(value = "UPDATE M_User SET is_locked = :lock WHERE email = :email", nativeQuery = true)
	@Modifying
	void updateIsLock(Boolean lock, String email);

}
