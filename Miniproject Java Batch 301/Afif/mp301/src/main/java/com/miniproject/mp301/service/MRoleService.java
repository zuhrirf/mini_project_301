package com.miniproject.mp301.service;

import java.util.List;

import com.miniproject.mp301.model.MRoleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.miniproject.mp301.repositories.MRoleRepository;

@Service
@Transactional
public class MRoleService {
	@Autowired
	private MRoleRepository mRoleRepository;
	
	public List<MRoleModel> getAllRole(){
		return mRoleRepository.findAll();
	}
}
