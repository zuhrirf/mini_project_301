package com.miniproject.mp301.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.miniproject.mp301.model.MCourierModel;
import com.miniproject.mp301.service.MCourierService;
@RestController
@RequestMapping("api")
public class MCourierRestController {
    @Autowired
    private MCourierService mcouService;
    @GetMapping("getallm_courier")
    public List<MCourierModel> getAllMCourier() {
        return mcouService.getAllMCourier();
    }
    @GetMapping("getm_courier/{id}")
    public MCourierModel getMCourierById(@PathVariable("id") Long id) {
        return mcouService.getMCourierById(id);
    }
    @PostMapping("addm_courier")
    public String addMCourier(@RequestBody MCourierModel cm) {List<MCourierModel> listNameCM = mcouService.cariName(cm.getName());
        if (listNameCM.size()>0) {return "Nama Yang Ditambahkan Sudah Dipakai!";}
        mcouService.addMCourier(cm);
        return "ok";
    }
    @PutMapping("editm_courier")
    public String editMCourier(@RequestBody MCourierModel cm) {

        MCourierModel cm0 = mcouService.getMCourierById(cm.getId());

        List<MCourierModel> listNameCM = mcouService.cariName(cm.getName());
        if (listNameCM.size()>0 && !cm.getName().equals(cm0.getName())) {
            return "Nama Yang Diedit Sudah Dipakai!";
        }

        mcouService.editMCourier(cm);
        return "ok";
    }

    @DeleteMapping("deletem_courier/{id}")
    public String deleteMCourier(@PathVariable("id") MCourierModel cm) {
        mcouService.deleteMCourier(cm.getId());
        return "ok";
    }
}
