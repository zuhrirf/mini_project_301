package com.miniproject.mp301.model;
import jakarta.persistence.*;

@Entity
@Table(name = "t_courier_discount")
public class TCourierDiscountModel {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, nullable = false, unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "courier_type_id", insertable = false, updatable = false)
    private MCourierTypeModel m_courier_type;

    @Column(nullable = false)
    private double courier_type_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MCourierTypeModel getM_courier_type() {
        return m_courier_type;
    }

    public void setM_courier_type(MCourierTypeModel m_courier_type) {
        this.m_courier_type = m_courier_type;
    }

    public double getCourier_type_id() {
        return courier_type_id;
    }

    public void setCourier_type_id(double courier_type_id) {
        this.courier_type_id = courier_type_id;
    }
}
