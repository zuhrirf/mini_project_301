package com.miniproject.mp301.model;
import jakarta.persistence.*;

@Entity
@Table(name = "m_courier_type")
public class MCourierTypeModel {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, nullable = false, unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "courier_id", insertable = false, updatable = false)
    private MCourierModel m_courier;

    @Column(nullable = false)//2
    private Long courier_id;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public MCourierModel getM_courier() {return m_courier;}

    public void setM_courier(MCourierModel m_courier) {this.m_courier = m_courier;}

    public Long getCourier_id() {return courier_id;}

    public void setCourier_id(Long courier_id) {this.courier_id = courier_id;}
}
