package com.miniproject.mp301.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.miniproject.mp301.model.MBiodataModel;
public interface MBiodataRepository extends JpaRepository<MBiodataModel, Long> {

}
