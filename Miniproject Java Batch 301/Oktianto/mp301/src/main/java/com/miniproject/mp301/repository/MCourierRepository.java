package com.miniproject.mp301.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.miniproject.mp301.model.MCourierModel;
public interface MCourierRepository extends JpaRepository<MCourierModel, Long> {
    @Query(value = "SELECT * FROM m_courier WHERE name = :cari", nativeQuery = true)
    List<MCourierModel> cariName(String cari);

}
