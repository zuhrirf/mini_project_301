package com.miniproject.mp301.model;
import jakarta.persistence.*;
import java.sql.Blob;

@Entity
@Table(name="m_biodata")
public class MBiodataModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(length = 255)
    private String fullname;

    @Column(length = 15)
    private String mobile_phone;

    @Lob
    private Blob image;

    @Column(length = 255)
    private String image_path;


    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getFullname() {return fullname;}

    public void setFullname(String fullname) {this.fullname = fullname;}

    public String getMobile_phone() {return mobile_phone;}

    public void setMobile_phone(String mobile_phone) {this.mobile_phone = mobile_phone;}

    public Blob getImage() {return image;}

    public void setImage(Blob image) {this.image = image;}

    public String getImage_path() {return image_path;}

    public void setImage_path(String image_path) {this.image_path = image_path;}
}
