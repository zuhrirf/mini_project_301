package com.miniproject.mp301.model;
import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "m_user")
public class MUserModel {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private MBiodataModel mbiodata;

    @Column(nullable = false)
    private Long biodata_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private MRoleModel mrole;

    @Column(nullable = false)
    private Long role_id;

    @Column(length = 100)
    private String email;

    @Column(length = 255)
    private String password;

    @Column
    private int login_attempt;

    @Column
    private Boolean is_locked;

    @Column(nullable = false)
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "Asia/Jakarta")
    private Date last_login;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public MBiodataModel getMbiodata() {return mbiodata;}

    public void setMbiodata(MBiodataModel mbiodata) {this.mbiodata = mbiodata;}

    public Long getBiodata_id() {return biodata_id;}

    public void setBiodata_id(Long biodata_id) {this.biodata_id = biodata_id;}

    public MRoleModel getMrole() {return mrole;}

    public void setMrole(MRoleModel mrole) {this.mrole = mrole;}

    public Long getRole_id() {return role_id;}

    public void setRole_id(Long role_id) {this.role_id = role_id;}

    public String getEmail() {return email;}

    public void setEmail(String email) {this.email = email;}

    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}

    public int getLogin_attempt() {return login_attempt;}

    public void setLogin_attempt(int login_attempt) {this.login_attempt = login_attempt;}

    public Boolean getIs_locked() {return is_locked;}

    public void setIs_locked(Boolean is_locked) {this.is_locked = is_locked;}

    public Date getLast_login() {return last_login;}

    public void setLast_login(Date last_login) {this.last_login = last_login;}
}
