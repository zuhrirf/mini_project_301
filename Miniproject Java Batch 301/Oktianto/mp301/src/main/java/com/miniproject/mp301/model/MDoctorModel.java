package com.miniproject.mp301.model;
import jakarta.persistence.*;

@Entity
@Table(name="m_doctor")
public class MDoctorModel {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private MBiodataModel biodata;

    private Long biodata_id;

    @Column(length = 50)
    private String str;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public MBiodataModel getBiodata() {return biodata;}

    public void setBiodata(MBiodataModel biodata) {this.biodata = biodata;}

    public Long getBiodata_id() {return biodata_id;}

    public void setBiodata_id(Long biodata_id) {this.biodata_id = biodata_id;}

    public String getStr() {return str;}

    public void setStr(String str) {this.str = str;}
}
