package com.miniproject.mp301.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.miniproject.mp301.model.MBiodataModel;
import com.miniproject.mp301.repository.MBiodataRepository;
@Service
@Transactional
public class MBiodataService {
    @Autowired
    private MBiodataRepository mbioRepo;
    public List<MBiodataModel> getAllMBiodata() {return mbioRepo.findAll();}
    public MBiodataModel getMBiodataById(Long id) {return mbioRepo.findById(id).get();}
    public void addMBiodata(MBiodataModel cm) {mbioRepo.save(cm);}
}
