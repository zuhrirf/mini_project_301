package com.miniproject.mp301.model;
import jakarta.persistence.*;

@Entity
@Table(name = "m_admin")
public class MAdminModel {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    private Long biodata_id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    private MBiodataModel mBiodataModel;

    @Column(length = 10)
    private String code;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public Long getBiodata_id() {return biodata_id;}

    public void setBiodata_id(Long biodata_id) {this.biodata_id = biodata_id;}

    public MBiodataModel getmBiodataModel() {return mBiodataModel;}

    public void setmBiodataModel(MBiodataModel mBiodataModel) {this.mBiodataModel = mBiodataModel;}

    public String getCode() {return code;}

    public void setCode(String code) {this.code = code;}
}
