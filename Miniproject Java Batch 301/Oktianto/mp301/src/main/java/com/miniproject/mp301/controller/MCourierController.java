package com.miniproject.mp301.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("m_courier")
public class MCourierController {
    @RequestMapping("")
    public String m_courier() {
        return "m_courier/m_courier";
    }
    @RequestMapping("addm_courier")
    public String addMCourier() {return "m_courier/addm_courier";}
    @RequestMapping("editm_courier/{id}")
    public String editMCourier(@PathVariable("id") Long id, Model model) {model.addAttribute("id", id);
        return "m_courier/editm_courier";
    }
    @RequestMapping("deletem_courier/{id}")
    public String deleteMCourier(@PathVariable("id") Long id, Model model) {model.addAttribute("id", id);
        return "m_courier/deletem_courier";
    }
}
