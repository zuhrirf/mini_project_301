package com.miniproject.mp301.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.miniproject.mp301.model.MCourierModel;
import com.miniproject.mp301.repository.MCourierRepository;
@Service
@Transactional
public class MCourierService {
    @Autowired
    private MCourierRepository mcouRepo;
    public List<MCourierModel> getAllMCourier() {return mcouRepo.findAll();}
    public MCourierModel getMCourierById(Long id) {return mcouRepo.findById(id).get();}
    public void addMCourier(MCourierModel cm) {mcouRepo.save(cm);}
    public void editMCourier(MCourierModel cm) {
        mcouRepo.save(cm);
    }
    public void deleteMCourier(Long id) {mcouRepo.deleteById(id);}
    public List<MCourierModel> cariName(String cari) {return mcouRepo.cariName(cari);}
}
