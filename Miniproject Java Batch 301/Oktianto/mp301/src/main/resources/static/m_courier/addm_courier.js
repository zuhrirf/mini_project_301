$("#addmcourierBtnCancel").click(function(){
	$(".modal").modal("hide")
})

$("#addmcourierBtnCreate").click(function(){
	//var initial = $("#initialInput").val();
	var name = $("#nameInput").val();
	//var active = $("#activeInput").is(":checked") ? true : false;
	
	/*if(initial == ""){
		$("#errInitial").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errInitial").text("");
	}*/
	if(name == ""){
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}
	
	var obj = {};
	//obj.initial = initial;
	obj.name = name;
	//obj.active = active;
	
	var myJson = JSON.stringify(obj);
	
	$.ajax({
		url : "/api/addm_courier",
		type : "POST",
		contentType : "application/json",
		data : myJson,
		success: function(data){
			if(data == "ok"){
				$(".modal").modal("hide")
				getAllMCourier();
			} else {
				$("#errName").text(data);
			}
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})