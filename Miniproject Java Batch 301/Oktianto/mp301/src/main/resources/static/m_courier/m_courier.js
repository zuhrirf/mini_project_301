function getAllMCourier(){
	$("#mcourierTable").html(
		`<thead>
			<tr>
				<th>NAMA</th>
				<th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><td> </td>
				<th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th>#########</th>
			</tr>
		</thead>
		<tbody id="mcourierTBody"></tbody>
		<thead>
            <tr>
                <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><td> </td>
                <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
                <th><<12>></th>
        	</tr>
        </thead>
		`
	);
	
	$.ajax({
		url : "/api/getallm_courier",
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#mcourierTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
						<td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
						<td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
						<td>
							<button value="${data[i].id}" onClick="editmcourier(this.value)" class="btn btn-primary">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deletemcourier(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>
					
					`
					
				)				
			}
		}
	});	
}
function getMCourierById(){
	$("#mcourierTable").html(
		`<thead>
			<tr>
				<th>NAMA</th>
				<th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><td> </td>
				<th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th>#########</th>
			</tr>
		</thead>
		<tbody id="mcourierTBody"></tbody>
		<thead>
            <tr>
                <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><td> </td>
                <th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th><th> </th>
                <th><<12>></th>
        	</tr>
        </thead>
		`
	);

	$.ajax({
		url: "/api/getm_courier/" + id,
		type : "GET",
		contentType : "application/json",
		success: function(data){
			for(i = 0; i<data.length; i++){
				$("#mcourierTBody").append(
					`
					<tr>
						<td>${data[i].name}</td>
						<td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
						<td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td><td> </td>
						<td>
							<button value="${data[i].id}" onClick="editmcourier(this.value)" class="btn btn-primary">
								<i class="bi-pencil-square"></i>
							</button>
							<button value="${data[i].id}" onClick="deletemcourier(this.value)" class="btn btn-danger">
								<i class="bi-trash"></i>
							</button>
						</td>
					</tr>

					`

				)
			}
		}
	});
}
$("#addBtn").click(function(){
	$.ajax({
		url: "/m_courier/addm_courier",
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("TAMBAH");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
})
$("#tampilenter").click(function(){
    var name = $("#namecari").val();
	if(name == ""){
		$("#cariName").text("Name masih kosong!");
		if(name == ""){
		$(document).ready(function(){
        	getAllMCourier();
        })
		return;
		}
	} else {
		$("#cariName").text("");
		$(document).ready(function(){
            getMCourierById();
        })
	}
})
function editmcourier(id){
	$.ajax({
		url: "/m_courier/editm_courier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("UBAH");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}

function deletemcourier(id){
	$.ajax({
		url: "/m_courier/deletem_courier/" + id,
		type: "GET",
		contentType: "html",
		success: function(data){
			$(".modal-title").text("HAPUS !");
			$(".modal-body").html(data);
			$(".modal").modal("show");
		}
	});
}
$(document).ready(function(){
	getMCourierById();
})
