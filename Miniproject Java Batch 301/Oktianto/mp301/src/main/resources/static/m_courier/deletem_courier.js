$(document).ready(function() {
	getMCourierById();

})

function getMCourierById() {
	var id = $("#delmcourierId").val();
	$.ajax({
		url: "/api/getm_courier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			//$("#initDel").text(data.initial);
			$("#nameDel").text(data.name);
			if (data.name) { $("#nameDel").prop("checked", true); }
		}
	})
}

$("#delmcourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#delmcourierBtnDelete").click(function() {
	var id = $("#delmcourierId").val();
	$.ajax({
		url : "/api/deletem_courier/"+ id,
		type : "DELETE",
		contentType : "application/json",
		success: function(data){
			if(data == "ok"){
				$(".modal").modal("hide")
				getAllMCourier();
			}
			
		},
		error: function(){
			alert("Terjadi kesalahan")
		}
	});
})