$(document).ready(function() {
	getMCourierById();

})

function getMCourierById() {
	var id = $("#editmcourierId").val();
	$.ajax({
		url: "/api/getm_courier/" + id,
		type: "GET",
		contentType: "application/json",
		success: function(data) {
			//$("#initEditInput").val(data.initial);
			$("#nameEditInput").val(data.name);
			if (data.name) { $("#nameEditInput").prop("checked", true); }
		}
	})
}

$("#editmcourierBtnCancel").click(function() {
	$(".modal").modal("hide")
})

$("#editmcourierBtnCreate").click(function() {
	var id = $("#editmcourierId").val();
	//var initial = $("#initEditInput").val();
	var name = $("#nameEditInput").val();
	//var active = $("#activeEditInput").is(":checked") ? true : false;
    /*
	if (initial == "") {
		$("#errInitial").text("Initial tidak boleh kosong!");
		return;
	} else {
		$("#errInitial").text("");
	}*/
	if (name == "") {
		$("#errName").text("Name tidak boleh kosong!");
		return;
	} else {
		$("#errName").text("");
	}

	var obj = {};
	obj.id = id;
	//obj.initial = initial;
	obj.name = name;
	//obj.active = active;

	var myJson = JSON.stringify(obj);

	$.ajax({
		url: "/api/editm_courier",
		type: "PUT",
		contentType: "application/json",
		data: myJson,
		success: function(data) {
			if (data == "ok") {
				$(".modal").modal("hide")
				getAllMCourier();
			}
			 else {
				$("#errName").text(data);
			}

		},
		error: function() {
			alert("Terjadi kesalahan")
		}
	});
})